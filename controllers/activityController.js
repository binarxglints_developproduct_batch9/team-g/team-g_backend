const {
    mongo
} = require('mongoose');
const {
    activity,
    project,
    task
} = require('../models');
const {
    ObjectId
} = require('mongodb')


class ActivityController {

    async getAll(req, res) {
        try {
            const data = await activity.find({});
            if (data) {
                return res.status(200).json({
                    status: "success",
                    message: "Success get Activity",
                    data: data
                });
            }
        } catch (err) {
            return res.status(500).json({
                status: "failed",
                message: "Internal Server Error"
            });
        }
    }
    async userActivity(req, res) {
        try {
            var page = parseInt(req.query.page)

            var limit = parseInt(req.query.limit)
            var query = {}

            query.skip = limit * (page - 1)
            query.limit = limit
            //const count = await movie.countDocuments();
            const dataProject = await project.find({
                $or: [{
                    'owner._id': new ObjectId(req.user.id)
                }, {
                    members: {
                        $elemMatch: {
                            _id: new ObjectId(req.user.id)
                        }
                    }
                }]
            })
            const countproject = await project.countDocuments({
                $or: [{
                    'owner._id': new ObjectId(req.user.id)
                }, {
                    members: {
                        $elemMatch: {
                            _id: new ObjectId(req.user.id)
                        }
                    }
                }]
            })
            let data = [];
            let countTask = 0;
            let countActivity = 0;
            if (dataProject) {
                for (const item of dataProject) {
                    const nTask = await task.countDocuments({
                        idProject: item._id
                    })
                    const nActivity = await activity.countDocuments({
                        "idProject": item._id
                    })
                    countTask += nTask;
                    countActivity += nActivity;
                    const getActivities = await activity.find({
                        "idProject": item._id
                    }, {
                        _id: 0,
                        idProject:1,
                        content: 1,
                        created_at: 1
                    }, query)

                    if (getActivities) {
                        for (const item of getActivities) {
                            const x = {
                                idProject:item.idProject,
                                content: item.content,
                                created_at: item.created_at
                            }
                            data.push(x)
                        }
                    }
                }
            }
            let countAll = {
                "countproject": countproject,
                "countTask": countTask,
                "countActivity": countActivity
            }
            if (data) {
                return res.status(200).json({
                    status: "success",
                    message: 'get data success.',
                    totalPages: Math.ceil(countActivity / limit),
                    currentPage: page,
                    countData: countAll,
                    data: data
                });
            }

        } catch (err) {
            // console.log(err);
            return res.status(422).json({
                message: "Get data failed",
                errors: err
            });
        }
    }

}

module.exports = new ActivityController();