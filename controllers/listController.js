const {
    user,
    list,
    project,
    task
} = require('../models') // import user models
const {
    ObjectId
} = require('mongodb')

class ListController {
    //GET ALL LIST
    async getAll(req, res) {
        try {
            const data = await list.find({})
            if (data) {
                return res.status(201).json({
                    "status": "success",
                    "message": "success for created payment, wait for verification by admin",
                    "data": data
                })
            }

        } catch (error) {
            return res.status(422).json({
                "status": "failed",
                "message": "Can't get a data",
                "data": error
            })

        }
    }
    //GET BY ID PROJECT
    async getByProject(req, res) {
        try {
            const data = await list.find({
                idProject: new ObjectId(req.params.idProject)
            })
            if (data) {
                return res.status(201).json({
                    "status": "success",
                    "message": "success for get list by project",
                    "data": data
                })
            }

        } catch (error) {
            return res.status(422).json({
                "status": "failed",
                "message": "Can't get a data",
                "data": error
            })
        }
    }

    //CREATE LIST
    async create(req, res) {
        try {
            const dataProject = await project.findOne({
                _id: new ObjectId(req.params.idProject)
            })
            const checkName = await list.findOne({
                'idProject': dataProject._id,
                'name': req.body.name
            })
            if (checkName) {
                return res.status(400).json({
                    "status": "failed",
                    "message": "Name already use",
                })
            } else {
                const countList = await list.countDocuments({
                    idProject: dataProject._id

                });
                // console.log(dataProject._id);
                const data = await list.create({
                    "name": req.body.name,
                    "idProject": dataProject._id,
                    "position": countList + 1
                })
                // console.log(data);
                const getDataList = await list.findOne({
                    idProject: dataProject._id,
                    'name': req.body.name
                })
                if (data) {
                    //push to activity
                    const subject = await user.findOne({
                        '_id': req.user.id
                    }, {
                        'fullName': 1
                    })
                    const dataActivity = await activity.create({
                        'idUser': new ObjectId(req.user.id),
                        'idProject': dataProject._id,
                        'content': `${subject.fullName} has create list '${getDataList.name}' in project '${dataProject.name}'`
                    })
                    /* SOCKET IO */
                    const getList = await list.find({
                        idProject: dataProject._id
                    })
                    let newList = {};
                    if (getList) {
                        for (const item of getList) {
                            const getTask = await task.find({
                                "idList._id": item._id
                            })
                            let newTask = {}
                            if (getTask) {
                                for (const item of getTask) {
                                    newTask[item._id] = {
                                        name: item.name,
                                        description: item.description,
                                        position: item.position,
                                        members: item.members,
                                        attachment: item.attachment
                                    }
                                }
                            }
                            // console.log(item);
                            const data = {
                                name: item.name,
                                position: item.position,
                                idProject: item.idProject,
                                tasks: newTask
                            }
                            newList[item._id] = data

                        }
                    }
                    req.io.to(dataProject._id.toString()).emit('updateBoard', newList);

                    /* END OF SOCKET IO */
                    return res.status(201).json({
                        "status": "success",
                        "message": "success for created List",
                        "data": data
                    })

                } else {
                    return res.status(422).json({
                        "status": "failed",
                        "message": "Can't create a data"
                    })
                }
            }
        } catch (e) {
            // console.log(e);
            return res.status(422).json({
                "status": "failed",
                "message": "Can't create a data",
                "data": e
            })
        }
    }
    async moveList(req, res) {
        try {
            const source = await list.findOne({
                '_id': new ObjectId(req.body.idList)
            });
            // console.log(source);
            const lists = await list.find({
                idProject: source.idProject
            })
            const destIndex = parseInt(req.body.destIndex);
            // console.log(destIndex);
            // console.log(source.position);
            for (const item of lists) {
                // console.log(item);
                if (item.position === source.position) {
                    await list.findOneAndUpdate({
                        _id: item._id
                    }, {
                        $set: {
                            'position': destIndex
                        }
                    }, {
                        new: true
                    })
                    // return;
                } else if (item.position < Math.min(source.position, destIndex) ||
                    item.position > Math.max(source.position, destIndex)) {
                    // console.log("cek 2");

                    // return;
                } else if (source.position < destIndex) {
                    // console.log("cekkk 3");
                    //item.position--;
                    await list.findOneAndUpdate({
                        _id: item._id
                    }, {
                        $set: {
                            'position': item.position - 1
                        }
                    })
                    // return;
                } else {
                    await list.findOneAndUpdate({
                        _id: item._id
                    }, {
                        $set: {
                            'position': item.position + 1
                        }
                    }, {
                        new: true
                    })
                }


            }

            const getList = await list.find({
                idProject: source.idProject
            })

            let newList = {};
            if (getList) {
                for (const item of getList) {
                    const getTask = await task.find({
                        "idList._id": item._id
                    })
                    let newTask = {}
                    if (getTask) {
                        for (const item of getTask) {
                            newTask[item._id] = {
                                name: item.name,
                                description: item.description,
                                position: item.position,
                                members: item.members,
                                attachment: item.attachment
                            }
                        }
                    }
                    // console.log(item);
                    const data = {
                        name: item.name,
                        position: item.position,
                        idProject: item.idProject,
                        tasks: newTask
                    }
                    newList[item._id] = data

                }
            }
            if (newList) {
                //PUSH TO ACTIVITY
                const subject = await user.findOne({
                    '_id': req.user.id
                })
                const getProject = await project.findById(source.idProject)
                const dataActivity = await activity.create({
                    'idUser': new ObjectId(req.user.id),
                    'idProject': source.idProject,
                    'content': `${subject.fullName} has moved list '${source.name}' in project '${getProject.name}' `
                })
                //SOCKET IO
                req.io.to(getProject._id.toString()).emit('updateBoard', newList);
                return res.status(200).json({
                    "status": "success",
                    "message": "success for move list",
                    "data": newList
                })
            } else {
                return res.status(422).json({
                    "status": "failed",
                    "message": "Can't get data tasks"
                })
            }

        } catch (error) {
            // console.log(error);
            return res.status(422).json({
                "status": "failed",
                "message": "Can't change a data",
                "data": error
            })

        }

    }
    async update(req, res) {
        try {
            const oldData = await list.findOne({
                _id: new ObjectId(req.params.idList)
            })
            const dataList = await list.findOneAndUpdate({
                _id: new ObjectId(req.params.idList)
            }, {
                $set: {
                    name: req.body.name
                }
            }, {
                new: true
            })

            if (dataList) {
                // console.log(data);
                const newData = await list.findOne({
                    _id: new ObjectId(req.params.idList)
                }, {
                    _id: 1,
                    name: 1
                })
                const dataTask = await task.updateMany({
                    'idList._id': new ObjectId(req.params.idList)
                }, {
                    $set: {
                        idList: newData
                    }
                })

                if (dataTask) {
                    //PUSH TO ACTIVITY
                    const subject = await user.findOne({
                        '_id': req.user.id
                    })
                    const getProject = await project.findById(dataList.idProject)
                    const dataActivity = await activity.create({
                        'idUser': new ObjectId(req.user.id),
                        'idProject': dataList.idProject,
                        'content': `${subject.fullName} changes list '${oldData.name}' to '${newData.name}' in project '${getProject.name}' `
                    })

                    /* SOCKET IO */
                    const getList = await list.find({
                        idProject: dataList.idProject
                    })
                    let newList = {};
                    if (getList) {
                        for (const item of getList) {
                            const getTask = await task.find({
                                "idList._id": item._id
                            })
                            let newTask = {}
                            if (getTask) {
                                for (const elem of getTask) {
                                    newTask[elem._id] = {
                                        name: elem.name,
                                        description: elem.description,
                                        position: elem.position,
                                        members: elem.members,
                                        attachment: elem.attachment
                                    }
                                }
                            }
                            // console.log(item);
                            const data = {
                                name: item.name,
                                position: item.position,
                                idProject: item.idProject,
                                tasks: newTask
                            }
                            newList[item._id] = data

                        }
                    }
                    req.io.to(getProject._id.toString()).emit('updateBoard', newList);

                    /* END OF SOCKET IO */

                    return res.status(200).json({
                        "status": "success",
                        "message": "success for update list",
                        "data": newList
                    })
                }

            }
        } catch (error) {
            console.log(error);
            return res.status(422).json({
                "status": "failed",
                "message": "Failed to update data",
                "data": error
            })
        }

    }
    async delete(req, res) {

        try {
            const dataList = await list.findOne({
                _id: new ObjectId(req.params.idList)
            })
            // console.log(dataList);
            const lists = await list.find({
                idProject: dataList.idProject
            })
            if (dataList) {
                const deleteAll = await Promise.all([
                    list.delete({
                        '_id': dataList._id
                    }),
                    task.delete({
                        'idList._id': dataList._id
                    })
                ])
                if (deleteAll) {
                    for (const item of lists) {
                        if (item.position > dataList.position) {
                            await list.findOneAndUpdate({
                                _id: item._id
                            }, {
                                $set: {
                                    position: item.position - 1
                                }
                            })
                        }
                    }
                    const subject = await user.findOne({
                        '_id': req.user.id
                    })
                    const getProject = await project.findById(dataList.idProject)
                    const dataActivity = await activity.create({
                        'idUser': new ObjectId(req.user.id),
                        'idProject': dataList.idProject,
                        'content': `${subject.fullName} deleted list '${dataList.name}' from project '${getProject.name}'`
                    })

                    /* SOCKET IO */
                    const getList = await list.find({
                        idProject: getProject._id
                    })
                    let newList = {};
                    if (getList) {
                        for (const item of getList) {
                            const getTask = await task.find({
                                "idList._id": item._id
                            })
                            let newTask = {}
                            if (getTask) {
                                for (const item of getTask) {
                                    newTask[item._id] = {
                                        name: item.name,
                                        description: item.description,
                                        position: item.position,
                                        members: item.members,
                                        attachment: item.attachment
                                    }
                                }
                            }
                            // console.log(item);
                            const data = {
                                name: item.name,
                                position: item.position,
                                idProject: item.idProject,
                                tasks: newTask
                            }
                            newList[item._id] = data

                        }
                    }

                    req.io.to(getProject._id.toString()).emit('updateBoard', newList);

                    /* END OF SOCKET IO */

                    return res.status(200).json({
                        "status": "success",
                        "message": "success for delete list and task",
                        "data": null
                    })
                } else {
                    return res.status(422).json({
                        "status": "failed",
                        "message": "Failed to delete data"
                    })
                }
            } else {
                return res.status(422).json({
                    "status": "failed",
                    "message": "ID List Not Found"
                })
            }

        } catch (error) {
            // console.log(error);
            return res.status(422).json({
                "status": "failed",
                "message": "Failed to delete data",
                "data": error
            })
        }
    }


}
module.exports = new ListController;