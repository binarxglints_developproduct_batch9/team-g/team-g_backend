const {
  user,
  list,
  project,
  task
} = require('../models') // import user models
const imagekit = require('../lib/imagekit')
const normalizeEmail = require('normalize-email')
const {
  ObjectId
} = require('mongodb');

class TaskController {
  async getAll(req, res) {
    try {
      const data = await task.find({

      })

      return res.status(200).json({
        status: "success",
        message: "Success to get Card",
        data: data
      })

    } catch (error) {
      // console.log(error);
      return res.status(422).json({
        status: "failed",
        message: "Failed get task",
        error: error
      })
    }
  }
  async moveTask(req, res) {
    try {
      const card = await task.findOne({
        _id: req.body.idTask
      });
      const sourceIdList = card.idList._id;
      // console.log(sourceIdList);
      const source = {
        idList: sourceIdList.valueOf().toString(),
        index: card.position
      };
      // console.log(source);
      const destination = {
        idList: req.body.destIdList,
        index: parseInt(req.body.destIndex)
      };
      // console.log(destination);
      //IN THE SAME LIST
      if (source.idList === destination.idList) {
        //find her friends in same list
        // console.log("cekkk");
        const tasks = await task.find({
          'idList._id': card.idList._id
        });
        // console.log(tasks);
        for (const item of tasks) {
          // console.log("cek for each");
          if (item.position == source.index) {
            //item.position = destination.index;
            await task.findOneAndUpdate({
              _id: item._id
            }, {
              $set: {
                position: destination.index
              }
            }, {
              new: true
            })
            // console.log("cek case 1");
            // return;

          } else if (item.position < Math.min(source.index, destination.index) ||
            item.position > Math.max(source.index, destination.index)) {
            // console.log("cek case 2");
            // return;
          } else if (source.index < destination.index) {
            // item.position--;
            await task.findOneAndUpdate({
              _id: item._id
            }, {
              $set: {
                position: item.position - 1
              }
            }, {
              new: true
            })
            // console.log("cek case 3");
            // return;
          }
          //item.position++;
          else {
            await task.findOneAndUpdate({
              _id: item._id
            }, {
              $set: {
                position: item.position + 1
              }
            }, {
              new: true
            })
          }
        }
      } else { //IN DIFFERENT LIST

        const sourceCards = await task.find({
          'idList._id': card.idList._id
        });
        // console.log("sourceCards" + sourceCards);
        const destinationCards = await task.find({
          'idList._id': new ObjectId(req.body.destIdList)
        })
        // console.log("destinationCards" + destinationCards);
        const movingCard = await task.findOne({
          _id: new ObjectId(req.body.idTask)
        });
        // console.log("movingCard" + movingCard);
        const destList = await list.findOne({
          _id: new ObjectId(req.body.destIdList)
        }, {
          _id: 1,
          name: 1
        })
        // console.log("destList:" + destList);
        const sourceList = await list.findOne({
          _id: movingCard.idList._id
        }, {
          _id: 1,
          name: 1
        })
        // console.log("sourceList:" + sourceList);
        sourceCards.forEach(async function (item) {
          if (item.position > source.index) {
            // item.position--;
            await task.findOneAndUpdate({
              _id: item._id
            }, {
              $set: {
                position: item.position - 1
              }
            }, {
              new: true
            })
            // console.log("cek case 3");
          }
        });
        destinationCards.forEach(async function (item) {
          if (item.position >= destination.index) {
            //item.position++;
            await task.findOneAndUpdate({
              _id: item._id
            }, {
              $set: {
                position: item.position + 1
              }
            }, {
              new: true
            })
            // console.log("cek case 3");
          }
        });
        //delete boardData[source.droppableId].cards[cardId];
        //movingCard.position = destination.index;
        const data1 = await task.findOneAndUpdate({
          _id: new ObjectId(req.body.idTask)
        }, {
          $set: {
            'idList._id': destList._id,
            'idList.name': destList.name,
            position: destination.index,
          }
        }, {
          new: true
        });

      }
      const getList = await list.find({
        idProject: card.idProject
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(card.idProject.toString()).emit('updateBoard', newList);

      return res.status(200).json({
        status: "success",
        message: "Success To Move Task",
        data: newList
      })

    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed To Move Task",
        error: error
      })
    }
  }


  //TASK CREATE
  async create(req, res) {
    try {
      const dataProject = await project.findOne({
        '_id': new ObjectId(req.params.idProject)
      })
      let idList = await list.findOne({
        'idProject': new ObjectId(req.params.idProject),
        'position': 1 // this is comment for position
      }, {
        '_id': 1,
        'name': 1
      })
      if (!idList) {
        await list.create({
          "name": "Backlog",
          "idProject": dataProject._id,
          "position": 1
        })

        idList = await list.findOne({
          'idProject': new ObjectId(req.params.idProject),
          'name': "Backlog"
        }, {
          '_id': 1,
          'name': 1
        })
      }

      const countList = await task.countDocuments({
        idProject: dataProject._id,
        idList: idList
      });
      let members = []
      if (req.body.members) {
        const emails = normalizeEmail(req.body.members);
        members = await user.find({
          email: emails
        }).select({
          password: 0,
          created_at: 0,
          deleted_at: 0,
          updated_at: 0,
          deleted: 0,
          bio: 0,
          subscription: 0,
          isVerified: 0
        })
        if (!members) {
          return res.status(422).json({
            status: "failed",
            message: "Failed create task, member is not registered!"
          })
        }
      }
      const data = await task.create({
        'name': req.body.name,
        'description': req.body.description,
        'idProject': dataProject._id,
        'idList': idList,
        'position': countList + 1,
        'members': members
      })

      // console.log(req.files);
      if (req.files) {
        for (const file of req.files) {
          // console.log(file);
          const uploadImage = await imagekit.upload({
            file: file.buffer,
            fileName: 'IMG_' + Date.now()
          });

          await task.findOneAndUpdate({
            _id: new ObjectId(data._id)
          }, {
            $push: {
              'attachment': uploadImage.url
            }
          }, {
            upsert: true
          });
        }
      }

      const getData = await task.findOne({
        _id: new ObjectId(data._id)
      })

      const subject = await user.findOne({
        _id: req.user.id
      })
      await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(getData.idProject),
        'content': `${subject.fullName} has created Task ${getData.name}`
      })

      const getList = await list.find({
        idProject: dataProject._id
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(dataProject._id.toString()).emit('updateBoard', newList);
      return res.status(200).json({
        status: "success",
        message: "Task has been created!",
        data: getData
      })

    } catch (e) {
      // console.log(e);
      return res.status(422).json({
        status: "failed",
        message: "Failed create task",
        error: e
      })
    }
  }
  // TASK GET ALL
  async getAllTask(req, res) {
    try {
      const getList = await list.find({
        idProject: new ObjectId(req.params.idProject)
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          // console.log("ini list" + item)
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              // console.log("ini task" + item)
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          // console.log("ini data list " + data)
          newList[item._id] = data

          // console.log("ini new list" + newList)

        }
      }
      // // console.log(datas);

      return res.status(200).json({
        status: "success",
        message: "Success to Get All Task For This Project!",
        data: newList
      })
    } catch (e) {
      // console.log(e);
      return res.status(422).json({
        status: "failed",
        message: "Failed to Get All Task For this Project!",
        error: e
      })
    }
  }

  // GET TASK
  async getTask(req, res) {
    try {
      const data = await task.find({
        _id: req.params.idTask
      })
      return res.status(200).json({
        status: "success",
        message: "Success to Get Task",
        data: data
      })
    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed to Get Task",
        error: e
      })
    }
  }
  // DELETE TASK
  async deleteTask(req, res) {
    try {
      const getTask = await task.findOne({
        _id: req.params.idTask
      })
      const getAllTask = await task.find({
        "idList._id": getTask.idList._id
      })

      const countTask = await task.countDocuments({
        'idList._id': getTask.idList._id
      })

      // console.log(countTask)   
      if (countTask == 1 || getTask.position == countTask) {
        await task.deleteById({
          _id: req.params.idTask
        })
      } else {
        for (let i = getTask.position + 1; i <= countTask; i++) {
          await task.findOneAndUpdate({
            "idList._id": getTask.idList._id,
            position: i
          }, {
            $set: {
              position: i - 1
            }
          })
        }
        await task.deleteById({
          _id: req.params.idTask
        })

      }


      const subject = await user.findOne({
        _id: req.user.id
      })
      await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(getTask.idProject),
        'content': `${subject.fullName} has deleted Task ${getTask.name}`
      })

      const getList = await list.find({
        idProject: getTask.idProject
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(getTask.idProject.toString()).emit('updateBoard', newList);

      return res.status(200).json({
        status: "success",
        message: "Success Delete Task",
        data: null
      })
      // req.io.to(req.body.idTask).emit('delete', data)


    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed Delete Task",
        error: e
      })
    }
  }

  //TASK UPDATE
  async update(req, res) {
    try {
      const updatedObj = {}

      if (req.body.name) updatedObj.name = req.body.name
      if (req.body.description) updatedObj.description = req.body.description

      const updateData = await task.findOneAndUpdate({
        _id: new ObjectId(req.params.idTask)
      }, {
        $set: updatedObj
      }, {
        new: true
      })

      // console.log(updateData);
      const subject = await user.findOne({
        _id: req.user.id
      })
      await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(updateData.idProject),
        'content': `${subject.fullName} has updated Task ${updateData.name}`
      })

      const getList = await list.find({
        idProject: updateData.idProject
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(updateData.idProject.toString()).emit('updateBoard', newList);

      return res.status(200).json({
        status: "success",
        message: "Success Updating Task",
        data: updateData
      })
    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed Updating Task",
        data: error
      })
    }
  }

  // UPLOAD ATTACHMENT
  async deleteAttachment(req, res) {
    try {
      const checkTask = await task.findOne({
        _id: req.params.idTask
      })
      if (!checkTask) {
        return res.status(422).json({
          status: "failed",
          message: "Failed to Delete Attachment",
          data: error
        })
      }

      await task.findOneAndUpdate({
        _id: req.params.idTask
      }, {
        $pull: {
          'attachment': req.body.attachment
        }
      })

      const datas = await task.findOne({
        _id: req.params.idTask
      }).select({
        _id: 1,
        attachment: 1
      })


      const subject = await user.findOne({
        _id: req.user.id
      })
      await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(checkTask.idProject),
        'content': `${subject.fullName} has deleted Attachment for Task ${checkTask.name}`
      })

      const getList = await list.find({
        idProject: checkTask.idProject
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(checkTask.idProject.toString()).emit('updateBoard', newList);

      return res.status(200).json({
        status: "success",
        message: "Success Attach File",
        data: datas
      })

    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed Attach File",
        data: error
      })
    }
  }


  async attachTask(req, res) {
    try {
      const idTask = req.params.idTask;
      if (req.files) {
        for (const file of req.files) {
          // console.log(file);
          const uploadImage = await imagekit.upload({
            file: file.buffer,
            fileName: 'IMG_' + Date.now()
          });

          await task.findOneAndUpdate({
            _id: new ObjectId(idTask)
          }, {
            $push: {
              'attachment': uploadImage.url
            }
          }, {
            upsert: true
          });
        }
      }
      const data = await task.findOne({
        _id: new ObjectId(idTask)
      }).select({
        _id: 0,
        attachment: 1
      })

      const datas = await task.findOne({
        _id: new ObjectId(idTask)
      })
      const subject = await user.findOne({
        _id: req.user.id
      })
      await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(datas.idProject),
        'content': `${subject.fullName} has updated Attachment for Task ${datas.name}`
      })

      const getList = await list.find({
        idProject: datas.idProject
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(datas.idProject.toString()).emit('updateBoard', newList);

      return res.status(200).json({
        status: "success",
        message: "Success Attach File",
        data: data
      })

    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed Attach File",
        data: error
      })
    }
  }

  //ASSIGN TASK
  async assignTask(req, res) {
    try {
      const emails = normalizeEmail(req.body.email);
      const getTask = await task.findOne({
        _id: req.params.idTask
      })

      const alreadyAssign = await task.findOne({
        _id: req.params.idTask,
        members: {
          $elemMatch: {
            email: emails
          }
        }
      })

      if (alreadyAssign) {
        return res.status(422).json({
          status: "failed",
          message: "Users has been assign in this task!",
        })
      }
      const isProjectOwner = await project.findOne({
        $and: [{
            _id: getTask.idProject
          },
          {
            'owner.email': req.user.email
          }
        ]
      })

      if (!isProjectOwner) {
        return res.status(422).json({
          status: "failed",
          message: "You're not allowed to assign member for this project. Please contact you project owner!",
        })
      }
      const getUser = await user.findOne({
        email: emails
      }).select({
        password: 0,
        created_at: 0,
        deleted_at: 0,
        updated_at: 0,
        deleted: 0,
        bio: 0,
        subscription: 0,
        isVerified: 0
      })
      if (!getUser) {
        return res.status(422).json({
          status: "failed",
          message: "User that you want to assign is not registered!"
        })
      }
      const isProjectMember = await project.findOne({
        $and: [{
            _id: getTask.idProject
          },
          {
            $or: [{
              'owner.email': emails
            }, {
              members: {
                $elemMatch: {
                  email: emails
                }
              }
            }]
          }
        ]
      })

      if (!isProjectMember) {
        return res.status(422).json({
          status: "failed",
          message: "This users is not a member of this project!",
          data: emails
        })
      }

      const update = await task.findOneAndUpdate({
        _id: req.params.idTask
      }, {
        $push: {
          'members': getUser
        }
      }, {
        upsert: true,
        new: true
      })

      const subject = await user.findOne({
        _id: req.user.id
      })
      await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(update.idProject),
        'content': `${subject.fullName} has been Assign to Task ${update.name}`
      })

      const getList = await list.find({
        idProject: update.idProject
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(update.idProject.toString()).emit('updateBoard', newList);

      if (update) {
        return res.status(200).json({
          status: "success",
          message: "Success Assign Member to Task",
          data: update
        })
      }
    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed to Assign Task",
        error: error
      })
    }
  }

  //REVOKE ASSIGN TASK
  async revokeAssignTask(req, res) {
    try {
      const emails = normalizeEmail(req.body.email);
      const getTask = await task.findOne({
        _id: req.params.idTask
      })

      const checkAssign = await task.findOne({
        _id: req.params.idTask,
        members: {
          $elemMatch: {
            email: emails
          }
        }
      })

      if (!checkAssign) {
        return res.status(422).json({
          status: "failed",
          message: "Users has not been assign in this task!",
        })
      }
      const isProjectOwner = await project.findOne({
        $and: [{
            _id: getTask.idProject
          },
          {
            'owner.email': req.user.email
          }
        ]
      })

      if (!isProjectOwner) {
        return res.status(422).json({
          status: "failed",
          message: "You're not allowed to revoke assign for this project. Please contact you project owner!",
        })
      }
      const getUser = await user.findOne({
        email: emails
      }).select({
        password: 0,
        created_at: 0,
        deleted_at: 0,
        updated_at: 0,
        deleted: 0,
        bio: 0,
        subscription: 0,
        isVerified: 0
      })
      if (!getUser) {
        return res.status(422).json({
          status: "failed",
          message: "User that you want to revoke is not registered!"
        })
      }
      const isProjectMember = await project.findOne({
        $and: [{
            _id: getTask.idProject
          },
          {
            $or: [{
              'owner.email': emails
            }, {
              members: {
                $elemMatch: {
                  email: emails
                }
              }
            }]
          }
        ]
      })

      if (!isProjectMember) {
        return res.status(422).json({
          status: "failed",
          message: "This users is not a member of this project!",
          data: emails
        })
      }

      await task.findOneAndUpdate({
        _id: req.params.idTask
      }, {
        $pull: {
          'members': getUser
        }
      })

      const updated = await task.findOne({
        _id: req.params.idTask
      })

      const subject = await user.findOne({
        _id: req.user.id
      })
      await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(updated.idProject),
        'content': `${subject.fullName} has updated Task ${updated.name}`
      })

      const getList = await list.find({
        idProject: updated.idProject
      })

      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const item of getTask) {
              newTask[item._id] = {
                name: item.name,
                description: item.description,
                position: item.position,
                members: item.members,
                attachment: item.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      //emit to client
      req.io.to(updated.idProject.toString()).emit('updateBoard', newList);

      if (updated) {
        return res.status(200).json({
          status: "success",
          message: "Success Revoke Member from Task",
          data: updated
        })
      }
    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed to Revoke Task",
        error: error
      })
    }
  }

  async moveTaskList(req, res) {
    try {
      const findTask = await task.findOne({
        _id: req.params.idTask
      })

      if (!findTask) {
        return res.status(404).json({
          status: "failed",
          message: "Not Found Task",
          error: error
        })
      }
      const getList = await list.findOne({
        idProject: findTask.idProject._id,
        name: req.body.listName
      }).select({
        name: 1
      })

      if (!getList) {
        return res.status(404).json({
          status: "failed",
          message: "Name of list not found!"
        })
      }

      const update = await task.findOneAndUpdate({
        _id: new ObjectId(req.params.idTask)
      }, {
        $set: {
          idList: getList
        }
      }, {
        new: true
      })


      if (update) {
        return res.status(200).json({
          status: "success",
          message: "Success update List!",
          data: update
        })
      }


    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed to Update List",
        error: error
      })
    }
  }
}
module.exports = new TaskController;