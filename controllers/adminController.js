const {
    admin
} = require('../models') // import admin models
const passport = require('passport'); // import passport
const jwt = require('jsonwebtoken'); // import jsonwebtoken
const jwt_decode = require('jwt-decode');
const bcrypt = require('bcrypt');

class AdminController {

    //SIGNUP ADMIN
    async signup(req, res) {
      
        admin.create({
            email : req.body.email,
            username : req.body.username,
            password : req.body.password
            
        }).then(result => {
            res.json({
                status: "success",
                message: 'Signup success!',
                // data : data
        })
         // create jwt token from body variable
        // const token = await jwt.sign({
        //     data: body
        // }, 'secret_password')

       
            // token: token
        })
    }
    // LOGIN ADMIN
    async login(admin, req, res) {
        // get the req.admin from passport authentication
        const body = {
            id: admin._id,
            email: admin.email,
            // role : admin.dataValues.role
        };

        // create jwt token from body variable
        const token = jwt.sign({
            admin: body
        }, 'secret_password')

        // success to create token
        res.status(200).json({
            status: "success",
            message: 'Login Success!',
            token: token
        })
    }


    //SHOW ALL ADMIN DATA
    async getAll(req, res) {
        //find all data
        admin.find({}).then(result => {
            res.json({
                status: 'success',
                data: result 
            })
        })
    }

    //SHOW ONE ADMIN DATA
    async getOne(req, res) {
        // Find one
         admin.findOne({
                _id: req.params.id
            }).select({
                _id: 0,
                password: 0,
                created_at: 0,
                deleted_at: 0,
                updated_at: 0,
                deleted: 0
            })
            .then(result => {
                res.json({
                    status: 'success get the admin data',
                    data: result
                })
            }).catch(err => {
                res.status(422).json({
                    status: 'failed',
                    message: err
                })
            })
    }

    // DELETE ADMIN
    async delete(req, res) {
        admin.delete({
          _id: req.params.id
        }).then(() => {
          res.json({
            status: "success",
            data: null
          })
        })
    }

}

module.exports = new AdminController;