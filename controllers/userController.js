const {
    user,
    task,
    project,
    payment
} = require('../models') // import user models
require('dotenv').config({
    path: `.env.${process.env.NODE_ENV}`
})
const passport = require('passport'); // import passport
const jwt = require('jsonwebtoken'); // import jsonwebtoken
const jwt_decode = require('jwt-decode');
const bcrypt = require('bcrypt');
const imagekit = require('../lib/imagekit');
const mailer = require('../lib/mailer')
const {
    ObjectId
} = require('mongodb')

const {
    OAuth2Client
} = require('google-auth-library');
const client = new OAuth2Client({
    clientID: process.env.GOOGLE_CLIENT_ID,
    clientSecret: process.env.GOOGLE_CLIENT_SECRET
});

class UserController {
    async getAll(req, res) {
        try {
            const data = await user.find({})
            if (data) {
                return res.status(200).json({
                    'status': "succes",
                    "message": "Success get all data",
                    "data": data
                })
            }
        } catch (error) {
            console.log(error);
            res.status(422).json({
                status: "failed"

            })
        }
    }

    async readToken(req, res) {
        try {
            const newToken = req.body.token
            const ticket = await client.verifyIdToken({
                idToken: newToken,
                audience: process.env.GOOGLE_CLIENT_ID
            });
            // console.log(ticket);
            const {
                name,
                email,
                picture
            } = ticket.getPayload()

            var userData = await user.findOne({
                'email': email
            })
            // console.log(userData);
            if (userData) {
                userData = await user.findOneAndUpdate({
                    email: email
                }, {
                    $set: {
                        isVerified: true
                    }
                }, {
                    new: true
                })
            } else {
                userData = await user.create({
                    fullName: name,
                    email: email,
                    image: picture,
                    password: 'defaultPassword',
                    isVerified: true
                })
            }
            //collect data id and email
            //console.log(userData);
            const body = {
                id: userData._id,
                email: userData.email
            };
            // create jwt token from body variable
            const token = jwt.sign({
                user: body
            }, 'secret_password')

            if (token) {
                return res.status(200).json({
                    status: "success",
                    message: 'signin google success',
                    token: token
                })
            } else {
                return res.status(404).json({
                    status: "failes",
                    message: 'Not Found'
                })
            }

        } catch (error) {
            //console.log(error);
            return res.status(422).json({
                status: "failed",
                message: "can't sign in with google"

            })
        }


    }

    /* user signup */
    async signup(data, req, res) {
        // console.log(user)
        try {
            const body = {
                fullName: data.fullName,
                email: data.email
            }

            // create jwt token from body variable
            const token = await jwt.sign({
                data: body
            }, 'secret_password')

            // Send Verification Email
            await mailer.verificationEmail(data, token)

            // response token to user
            return res.status(200).json({
                token: token,
                status: "success",
                message: 'SignUp success! Please check your email',
                token: token
            });
        } catch (error) {
            return res.status(422).json({
                status: "failed",
                message: 'Failed SignUp!',
            });
        }

    }

    /* user sign in */
    async login(user, req, res) {
        // get the req.user from passport authentication
        const body = {
            id: user._id,
            email: user.email,
            // role : user.dataValues.role
        };

        // create jwt token from body variable
        const token = jwt.sign({
            user: body
        }, 'secret_password')

        // success to create token
        res.status(200).json({
            status: "success",
            message: 'Login Success!',
            token: token
        })
    }


    /* email verification */
    async verifyUser(req, res) {

        try {
            const token = req.params.token;
            const payload = await jwt_decode(token);
            // console.log(payload)
            const checkUser = await user.findOne({
                email: payload.data.email
            });

            if (checkUser) {
                await user.updateOne({
                    email: payload.data.email
                }, {
                    $set: {
                        isVerified: true
                    }
                })

                return res.status(200).json({
                    status: "success",
                    message: "Your email has been verified!"
                })
            } else {
                return res.status(404).json({
                    status: 'failed',
                    message: 'User Not Found'
                })
            }
        } catch (error) {
            return res.status(422).json({
                status: 'failed',
                message: 'Invalid token'
            })
        }
    }

    // Get one user
    async getOne(req, res) {
        // Find one
        await user.findOne({
                _id: req.user.id
            }).select({
                password: 0,
            })
            .then(result => {
                res.json({
                    status: 'success get the user data',
                    data: result
                })
            }).catch(e => {
                res.status(422).json({
                    status: 'failed',
                    message: "failed get user data"
                })
            })
    }

    async update(req, res) {
        try {
            let updatedObj = {}
            if (req.file) {
                const image = await imagekit.upload({
                    file: req.file.buffer,
                    fileName: 'IMG_' + Date.now()
                })
                updatedObj.image = image.url

            }

            if (req.body.fullName) updatedObj.fullName = req.body.fullName
            if (req.body.bio) updatedObj.bio = req.body.bio

            const dataUser = await user.findOneAndUpdate({
                _id: new ObjectId(req.user.id)
            }, {
                $set: updatedObj
            }, {
                new: true
            }).select({
                _id: 1,
                email: 1,
                fullName: 1,
                image: 1
            })
            const getDataOwner = await project.find({
                'owner._id': dataUser._id
            })
            if (getDataOwner) {
                await project.updateMany({
                    'owner._id': dataUser._id
                }, {
                    $set: {
                        owner: dataUser
                    }
                })
            }
            const getdataMemberProject = await project.find({
                members: {
                    $elemMatch: {
                        _id: dataUser._id
                    }
                }
            })
            if (getdataMemberProject) {
                await project.updateMany({
                    members: {
                        $elemMatch: {
                            _id: dataUser._id
                        }
                    }
                }, {
                    '$set': {
                        'members.$': dataUser,
                    }

                })
            }

            const getdataMemberTask = await task.find({
                members: {
                    $elemMatch: {
                        _id: dataUser._id
                    }
                }
            })
            if (getdataMemberTask) {
                await task.updateMany({
                    members: {
                        $elemMatch: {
                            _id: dataUser._id
                        }
                    }
                }, {
                    '$set': {
                        'members.$': dataUser,
                    }

                })
            }


            // console.log(dataProject_owner)
            const getPayment = await payment.find({
                'user._id': dataUser._id
            })

            if (getPayment) {
                await payment.updateMany({
                    'user._id': dataUser._id
                }, {
                    $set: {
                        user: dataUser
                    }
                })
            }
            const data = await user.findOne({
                '_id': dataUser._id
            }).select({
                password: 0
            })

            return res.status(200).json({
                status: "success",
                message: "Success updating data!",
                data: data
            })
        } catch (error) {
            console.log(error)
            return res.status(422).json({
                status: "failed",
                message: "Failed to update data!",
                data: null
            })
        }

    }

    async changePassword(req, res) {
        try {
            const body = {
                oldPassword: req.body.oldPassword,
                newPassword: req.body.newPassword,
                reEnterPassword: req.body.reEnterPassword
            }
            await user.findOneAndUpdate({
                _id: new ObjectId(req.user.id)
            }, {
                $set: {
                    password: body.newPassword
                }
            }, {
                new: true
            })

            return res.status(200).json({
                status: "success",
                message: 'Change password sucessfull'
            })
        } catch (error) {
            return res.status(422).json({
                status: "failed",
                message: 'Failed to change password!',
            })
        }


    };

    /**GOOGLE API SIDE */

    //signin with google
    async signInGoogle(req, res) {
        //console.log(req.user);

        //collect data id and email
        const body = {
            id: req.user.id,
            email: req.user.email
        };
        // create jwt token from body variable
        const token = jwt.sign({
            user: body // payload
        }, 'secret_password')

        const passwordDefault = 'defaultPassword'

        //check password in user account. still default => Go To Set Password
        bcrypt.compare(passwordDefault, req.user.password, function (err, result) {
            if (err) {
                // handle error
                return res.json({
                    message: err
                })
            }
            if (result) {
                // password masih default need to set
                return res.status(201).json({
                    status: "SetPassword",
                    message: 'Login by google Success! Please set your password',
                    token: token
                })
            } else {
                // response is OutgoingMessage object that server response http request
                return res.status(200).json({
                    status: "Dashboard",
                    message: 'Login by google Success! Go to the dashboard project',
                    token: token
                })
            }
        });
    }

    //set password
    async setPassword(req, res) {
        try {
            //cek token from google sigin
            const token = req.params.token;
            //decode token
            const payload = await jwt_decode(token);
            //console.log(payload)

            // //collect data for new token
            // const body = {
            //     id: payload.user.id,
            //     email: payload.user.email,
            //     // role : user.dataValues.role
            // };

            // // create jwt token from body variable
            // const newToken = jwt.sign({
            //     user: body
            // }, 'secret_password')

            //find and update password
            const checkUser = await user.findOneAndUpdate({
                email: payload.user.email
            }, {
                $set: {
                    password: req.body.password
                }
            }, {
                new: true
            })

            // success to create token
            return res.status(200).json({
                status: "success",
                message: 'You have created password!',
                // data:checkUser,
                token: token
            })

        } catch (e) {
            return res.status(422).json({
                "status": 'failed',
                "message": "failed to set password",
                "data": null
            })
        }

    }

}
module.exports = new UserController;