const {
  user,
  project,
  list,
  task,
  activity
} = require('../models') // import user models
const {
  ObjectId
} = require('mongodb')
const imagekit = require('../lib/imagekit')
const randomImage = require('../lib/randomImage');
const mailer = require('../lib/mailer')

class ProjectController {

  //GET ALL
  async getAll(req, res) {
    try {
      const data = await project.find({})
      if (data) {
        return res.status(200).json({
          'status': "success",
          "message": "Success get all data",
          "data": data
        })
      }
    } catch (e) {
      return res.status(422).json({
        "status": "failed",
        "message": "Retrieve Data Failed",
        "data": null
      })
    }
  }

  //PROJECT User's Project
  async userProject(req, res) {
    try {
      //console.log(req.user);
      const data = await project.find({
        $or: [
          // {'owner._id':new ObjectId(req.params.idUser)
          {
            'owner._id': new ObjectId(req.user.id)
          },
          // {"members":new ObjectId(req.params.idUser)
          {
            members: {
              $elemMatch: {
                _id: new ObjectId(req.user.id)
              }
            }
          }
        ]
      }, {})
      return res.status(200).json({
        "status": "success",
        "message": "succes get user's project",
        "data": data
      })
    } catch (e) {
      return res.status(422).json({
        "status": "failed",
        "message": "Retrieve Data Failed",
        "data": null
      })
    }
  }

  //GET ONE
  async getOne(req, res) {
    try {

      //check idProject and check ownership
      const dataProject = await project.findOne({
        $and: [{
            "_id": new ObjectId(req.params.idProject)
          },
          {
            $or: [
              //user who have assigned to be owner or member can access the project
              {
                'owner._id': new ObjectId(req.user.id)
              },
              {
                members: {
                  $elemMatch: {
                    _id: new ObjectId(req.user.id)
                  }
                }
              }
            ]
          }
        ]
      })
      // .populate("owner")

      if (dataProject) {
        return res.status(200).json({
          "status": "success",
          "message": "success for get one data",
          "data": dataProject
        })
      } else {
        return res.status(404).json({
          "status": "failed",
          "message": "Data Not Found"
        })
      }

    } catch (e) {
      // console.log(e);
      return res.status(422).json({
        "status": "failed",
        "message": "Retrieve Data Failed",
        "data": null
      })

    }
  }

  //PROJECT CREATE
  async create(req, res) {
    try {
      const countProject = await project.countDocuments({
        'owner.email': req.user.email
      })
      const getUser = await user.findById(req.user.id)

      if (getUser.subscription == false && countProject >= 5) {
        return res.status(422).json({
          "status": "failed",
          "message": "You're basic user. Kindly upgrade to premium and create your own project without limit."
        })
      }
      let images
      if (req.file) {
        const uploadImage = await imagekit.upload({
          file: req.file.buffer,
          fileName: 'IMG_' + Date.now()
        })
        images = uploadImage.url
      } else {
        images = randomImage()
      }
      const owner = await user.findOne({
        '_id': req.user.id
      }, {
        '_id': 1,
        'fullName': 1,
        'email': 1,
        'image': 1
      })

      const member = []
      const data = await project.create({
        'name': req.body.name,
        'description': req.body.description,
        'owner': owner,
        'bgImage': images,
        'members': member
      })

      const nameOfList = ["Backlog", "To-Do", "On-Progress", "Done"]
      for (var i = 0; i < nameOfList.length; i++) {
        await list.create({
          "name": nameOfList[i],
          "idProject": data._id,
          "position": i + 1
        })
      }
      //console.log(data);
      const subject = await user.findOne({
        '_id': req.user.id
      }, {
        'fullName': 1,
      })
      const dataActivity = await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(data._id),
        'content': `${subject.fullName} created  new project '${data.name}'`
      })

      // console.log(dataActivity);
      return res.status(201).json({
        "status": "success",
        "message": "success for created project",
        "data": data
      })

    } catch (e) {
      return res.status(422).json({
        "status": "failed",
        "message": "Can't create a data",
        "data": null
      })
    }
  }

  //UPDATE DATA PROJECT
  async update(req, res) {
    try {
      let updatedObj = {}
      if (req.body.name) updatedObj.name = req.body.name
      if (req.body.description) updatedObj.description = req.body.description
      if (req.file) {
        const uploadImage = await imagekit.upload({
          file: req.file.buffer,
          fileName: 'IMG_' + Date.now()
        })
        updatedObj.bgImage = uploadImage.url
      }

      const data = await project.findOneAndUpdate({
        $and: [{
            "_id": new ObjectId(req.params.idProject)
          },
          {
            $or: [
              //user who have assigned to be owner or member can access the project
              {
                'owner._id': new ObjectId(req.user.id)
              },
              {
                members: {
                  $elemMatch: {
                    _id: new ObjectId(req.user.id)
                  }
                }
              }
            ]
          }
        ]
      }, {
        $set: updatedObj
      }, {
        new: true
      })
      const subject = await user.findOne({
        '_id': req.user.id
      }, {
        'fullName': 1
      })
      const dataActivity = await activity.create({
        'idUser': new ObjectId(req.user.id),
        'idProject': new ObjectId(data._id),
        'content': `${subject.fullName} updated '${data.name}'`
      })
      if (dataActivity) {
        return res.status(200).json({
          "status": "success",
          "message": "data has been updated",
          "data": data
        })
      }

    } catch (e) {
      return res.status(422).json({
        "statu": "failed",
        "message": "Can't update a data",
        "error": e
      })
    }
  }


  //DELETE DATA PROJECT => ONLY OWNER
  async delete(req, res) {
    try {
      const checkAccess = await project.findOne({
        "_id": new ObjectId(req.params.idProject),
        'owner.email': req.user.email

      })
      if (!checkAccess) {
        return res.status(422).json({
          "status": "failed",
          "message": "You can't delete this project",
          "data": null
        })
      } else {
        //REMOVE IN COLLECTION PROJECT,LIST AND TASK
        const data = await Promise.all([
          project.delete({
            $and: [{
                "_id": new ObjectId(req.params.idProject)
              },
              {
                'owner._id': new ObjectId(req.user.id)
              },
            ]
          }),
          list.delete({
            'idProject': new ObjectId(req.params.idProject)
          }),
          task.delete({
            'idProject': new ObjectId(req.params.idProject)
          })
        ])

        const subject = await user.findOne({
          'email': req.user.email
        }, {
          'fullName': 1
        })

        const dataActivity = await activity.create({
          'idUser': new ObjectId(req.user.id),
          'idProject': new ObjectId(data._id),
          'content': `${subject.fullName} has deleted ${checkAccess.name}`
        })
        //console.log(dataActivity);
        if (data) {
          return res.status(200).json({
            "status": "success",
            "message": "data has been deleted",
            "data": null
          })
        } else {
          return res.status(422).json({
            "status": "failed",
            "message": "Can't delete a data"
          })
        }
      }

    } catch (e) {
      // console.log(e);
      return res.status(422).json({
        "status": "failed",
        "message": "Can't delete a data",
        "error": e
      })
    }
  }



  //SHOW MEMBERS OF THE Project
  async showMembersAndOwner(req, res) {
    try {
      const data = await project.findOne({
        '_id': new ObjectId(req.params.idProject)
      }, {
        'owner': 1,
        'members': 1
      })

      return res.status(200).json({
        "status": "status",
        'message': "show all members",
        'data': data
      })

    } catch (e) {
      return res.status(422).json({
        "status": "failed",
        "message": "Can't get data members",
        "error": e
      })
    }
  }
  //INVITE MEMBER TO THE PROJECT
  async inviteMember(req, res) {
    try {
      //collect dataUserInvited
      const userInvited = await user.findOne({
        "email": req.body.email
      }, {
        '_id': 1,
        'fullName': 1,
        'email': 1,
        'image': 1
      })

      //cek validation email
      const validateEmail = await project.findOne({
        $and: [{
            '_id': new ObjectId(req.params.idProject)
          },
          {
            $or: [
              //user who have assigned to be owner or member can access the project
              {
                'owner.email': req.user.email
              },
              {
                members: {
                  $elemMatch: {
                    email: req.user.email
                  }
                }
              }
            ]
          }
        ]
      })
      if (validateEmail) {
        //check if data exist
        const checkDataMember = await project.findOne({
          $and: [{
              '_id': new ObjectId(req.params.idProject)
            },
            {
              $or: [
                //chcek in database
                {
                  'owner.email': userInvited.email
                },
                {
                  members: {
                    $elemMatch: {
                      email: userInvited.email
                    }
                  }
                }
              ]
            }
          ]
        })
        //if data data exist
        if (checkDataMember) {
          res.status(422).json({
            "status": "failed",
            "message": "this email has been join this project"
          })
        } else {
          //find and insert members
          const data = await project.findOneAndUpdate({
            '_id': new ObjectId(req.params.idProject),
          }, {
            $push: {
              'members': userInvited
            }
          }, {
            upsert: true,
            new: true
          })
          mailer.inviteMember(req.user.email, userInvited.email, userInvited.fullName, data.name)
          //push to activity
          const subject = await user.findOne({
            '_id': req.user.id
          }, {
            'fullName': 1
          })
          const dataActivity = await activity.create({
            'idUser': new ObjectId(req.user.id),
            'idProject': new ObjectId(data._id),
            'content': `${subject.fullName} invited ${userInvited.fullName} to join ${data.name}`
          })
          if (dataActivity) {
            return res.status(200).json({
              "status": "success",
              "message": "success to invite member",
              "data": data
            })
          } else {
            return res.status(422).json({
              "status": "failed",
              "message": "failed add to activity"
            })
          }
        }


      } else {
        return res.status(422).json({
          "status": "failed",
          "message": "You don't have access"
        })

      }

    } catch (e) {
      return res.status(422).json({
        "status": "failed",
        "message": "Can't invite member",
        "error": e
      })
    }

  }


  //KICK MEMBER FROM PROJECT
  async kickMember(req, res) {
    try {
      //cek validation email
      const validateEmail = await project.findOne({
        '_id': new ObjectId(req.params.idProject),
        'owner.email': req.user.email
      })

      if (validateEmail) {
        //collect dataUserInvited
        const userKicked = await user.findOne({
          "email": req.body.email
        }, {
          '_id': 1,
          'fullName': 1,
          'email': 1,
          'image': 1

        })


        //find and kick member
        const data = await Promise.all([
          project.findOneAndUpdate({
            '_id': new ObjectId(req.params.idProject)
          }, {
            $pull: {
              'members': userKicked
            }
          }, {
            new: true
          }),
          task.updateMany({
            'idProject': new ObjectId(req.params.idProject),
            members: {
              $elemMatch: {
                _id: userKicked._id
              }
            }

          }, {
            $pull: {
              'members': userKicked
            }
          }, {
            new: true
          })
        ])

        if (data) {

          mailer.kickMember(userKicked.email, userKicked.fullName, data.name)

          //push to activity
          const subject = await user.findOne({
            '_id': req.user.id
          }, {
            'fullName': 1
          })
          const dataActivity = await activity.create({
            'idUser': new ObjectId(req.user.id),
            'idProject': new ObjectId(data._id),
            'content': `${userKicked.fullName} has kicked from ${data[0].name}`
          })
          //console.log(dataActivity);
          if (dataActivity) {
            /* SOCKET IO */
            const getList = await list.find({
              idProject: validateEmail._id
            })
            let newList = {};
            if (getList) {
              for (const item of getList) {
                const getTask = await task.find({
                  "idList._id": item._id
                })
                let newTask = {}
                if (getTask) {
                  for (const elem of getTask) {
                    newTask[elem._id] = {
                      name: elem.name,
                      description: elem.description,
                      position: elem.position,
                      members: elem.members,
                      attachment: elem.attachment
                    }
                  }
                }
                // console.log(item);
                const data = {
                  name: item.name,
                  position: item.position,
                  idProject: item.idProject,
                  tasks: newTask
                }
                newList[item._id] = data

              }
            }
            req.io.to(validateEmail._id.toString()).emit('updateBoard', newList);

            req.io.to(validateEmail._id.toString()).emit('updateProject', data[0]);
            /* END OF SOCKET IO */

            return res.status(200).json({
              "status": "success",
              "message": "success to kick member",
              "data": data[0]
            })
          } else {
            res.status(422).json({
              "status": "failed",
              "message": `failed to add to activity`
            })
          }
        } else {
          res.status(422).json({
            "status": "failed",
            "message": `failed to kick member from project`
          })
        }
      } else {
        res.status(422).json({
          "status": "failed",
          "message": `You don't have access to kick member`
        })
      }

    } catch (error) {
      // console.log(error);
      return res.status(422).json({
        "status": "failed",
        "message": "Can't kick member",
      })
    }
  }

  //INVITE MEMBER FROM ARRAY
  async inviteMembers(req, res) {
    const getProject = await project.findOne({
      _id: new ObjectId(req.params.idProject)
    })
    const input = req.body.email
    const data = input.split(",")
    // console.log(data);
    let temp = [];
    let emailNotRegister = [];
    for (const item of data) {
      const getUser = await user.findOne({
        email: item
      }, {
        _id: 1,
        fullName: 1,
        image: 1,
        email: 1
      })
      if (getUser) {
        const alreadyMember = await project.findOne({
          $and: [{
            _id: getProject._id,
          }, {
            $or: [{
                'owner.email': item
              },
              {
                members: {
                  $elemMatch: {
                    email: item
                  }
                }
              }
            ]
          }]
        })
        if (alreadyMember) {
          temp.push(item)
        }
        if (!alreadyMember) {
          const update = await project.findOneAndUpdate({
            _id: getProject._id
          }, {
            $push: {
              'members': getUser
            }
          }, {
            upsert: true
          });
          if (update) {
            //push to activity
            const subject = await user.findOne({
              '_id': req.user.id
            }, {
              'fullName': 1
            })
            //send email
            mailer.inviteMember(subject.fullName, item, getUser.fullName, getProject.name)

            const dataActivity = await activity.create({
              'idUser': new ObjectId(req.user.id),
              'idProject': new ObjectId(getProject._id),
              'content': `${subject.fullName} invites ${getUser.fullName} to join ${getProject.name}`
            })
          }

        }
      }
      if (!getUser) {
        emailNotRegister.push(item)
      }

    }
    const updated = await project.findOne({
      _id: getProject._id
    })
    /* SOCKET IO */
    const getList = await list.find({
      idProject: updated._id
    })
    let newList = {};
    if (getList) {
      for (const item of getList) {
        const getTask = await task.find({
          "idList._id": item._id
        })
        let newTask = {}
        if (getTask) {
          for (const elem of getTask) {
            newTask[elem._id] = {
              name: elem.name,
              description: elem.description,
              position: elem.position,
              members: elem.members,
              attachment: elem.attachment
            }
          }
        }
        // console.log(item);
        const data = {
          name: item.name,
          position: item.position,
          idProject: item.idProject,
          tasks: newTask
        }
        newList[item._id] = data

      }
    }
    req.io.to(updated._id.toString()).emit('updateBoard', newList);

    /* END OF SOCKET IO */
    if (emailNotRegister.length == data.length) {
      return res.status(400).json({
        status: "failed",
        message: "This email might be not registered"
      })
    }
    if (temp.length == data.length) {
      return res.status(400).json({
        status: "failed",
        message: "This email has join the project"
      })
    }
    if (temp.length > 0) {

      req.io.to(updated._id.toString()).emit('updateProject', updated);

      return res.status(200).json({
        status: "success",
        message: `success. but ${temp},${emailNotRegister} can't add to the project`,
        data: updated
      })
    }
    if (temp.length == 0 || temp.length == null) {
      req.io.to(updated._id.toString()).emit('updateProject', updated);
      return res.status(200).json({
        status: "success",
        message: `success for invite member`,
        data: updated
      })
    }
  }
  catch (error) {
    return res.status(422).json({
      status: "failed",
      message: "Failed To Invite Member Into Project",
      error: error
    })
  }

  //LEAVE PROJECT
  async leave(req, res) {
    try {

      const dataUser = await user.findOne({
        "_id": req.user.id
      }, {
        '_id': 1,
        'fullName': 1,
        'email': 1,
        'image': 1

      })

      const dataProject = await project.findOneAndUpdate({
        '_id': new ObjectId(req.params.idProject)
      }, {
        $pull: {
          'members': dataUser
        }
      }, {
        new: true
      })
      const dataTask = await task.find({
        'idProject': new ObjectId(req.params.idProject)
      })
      if (dataTask) {

        await task.updateMany({
          'idProject': new ObjectId(req.params.idProject),
          members: {
            $elemMatch: {
              _id: dataUser._id
            }
          }

        }, {
          $pull: {
            'members': dataUser
          }
        }, {
          new: true
        })
      }
      /* SOCKET IO */
      const getList = await list.find({
        idProject: dataProject._id
      })
      let newList = {};
      if (getList) {
        for (const item of getList) {
          const getTask = await task.find({
            "idList._id": item._id
          })
          let newTask = {}
          if (getTask) {
            for (const elem of getTask) {
              newTask[elem._id] = {
                name: elem.name,
                description: elem.description,
                position: elem.position,
                members: elem.members,
                attachment: elem.attachment
              }
            }
          }
          // console.log(item);
          const data = {
            name: item.name,
            position: item.position,
            idProject: item.idProject,
            tasks: newTask
          }
          newList[item._id] = data

        }
      }
      req.io.to(dataProject._id.toString()).emit('updateBoard', newList);

      req.io.to(dataProject._id.toString()).emit('updateProject', dataProject);
      /* END OF SOCKET IO */

      return res.status(200).json({
        status: "success",
        message: `success for leave from project`,
        data: data
      })

    } catch (error) {
      return res.status(422).json({
        status: "failed",
        message: "Failed To Leave from Project",
        error: error
      })
    }
  }



}

module.exports = new ProjectController;