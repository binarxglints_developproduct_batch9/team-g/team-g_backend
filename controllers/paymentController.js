const {
    user,
    payment
} = require('../models') // import user models
const {
    ObjectId
} = require('mongodb')
const imagekit = require('../lib/imagekit')

class PaymentController {
    //GET ALL
    async getAll(req, res) {
        try {
            const data = await payment.find({})
            if (data) {
                return res.status(201).json({
                    "status": "success",
                    "message": "success for get all payment ",
                    "data": data
                })
            }

        } catch (error) {
            return res.status(422).json({
                "status": "failed",
                "message": "Can't create a data",
                "data": e
            })

        }
    }
    //CREATE MEANS UPLOAD PAYMENT EVIDENCE
    async create(req, res) {
        try {
            let images
            if (req.file) {
                const uploadImage = await imagekit.upload({
                    file: req.file.buffer,
                    fileName: 'IMG_' + Date.now()
                });
                images = uploadImage.url;
            }
            // console.log(req.user);
            const dataUser = await user.find({
                '_id': req.user.id
            }, {
                '_id': 1,
                'fullName': 1,
                'email': 1,
                'image': 1
            })
            //cek dataPayment have exist or not
            var dataPayment = await payment.findOne({
                'user.email': dataUser[0].email,
            })
            if (dataPayment) {
                dataPayment = await payment.findOneAndUpdate({
                    '_id': dataPayment._id,
                }, {
                    $set: {
                        'image': images,
                        'status': "pending",
                        'notes': "[user just uploads payment evidence]"
                    }
                }, {
                    new: true
                })
            } else {

                dataPayment = await payment.create({
                    'user': dataUser[0],
                    'image': images,
                    'status': "pending",
                    'notes': "{user just upload, please check}"

                })
                // console.log(dataPayment);
            }
            return res.status(201).json({
                "status": "success",
                "message": "success for created payment, wait for verification by admin",
                "data": dataPayment
            })

        } catch (e) {
            return res.status(422).json({
                "status": "failed",
                "message": "Can't create a data",
                "data": e
            })
        }
    }

    //VERIFY 
    async verify(req, res) {
        try {
            const data = await user.findOneAndUpdate({
                '_id': req.body.idUser
            }, {
                $set: {
                    'subscription': true
                }
            }, {
                new: true
            })

            let notes
            if (req.body.notes) {
                notes = req.body.notes
            }
            const dataPayment = await payment.findOneAndUpdate({
                'user._id': data._id,
            }, {
                $set: {
                    'status': 'done',
                    'notes': notes
                }
            }, {
                new: true
            })

            return res.status(201).json({
                "status": "success",
                "message": "success for created payment, wait for verification by admin",
                "data": dataPayment
            })
        } catch (error) {
            return res.status(422).json({
                "status": "failed",
                "message": "Can't create a data",
                "data": error
            })

        }

    }
    //DECLINE
    async decline(req, res) {
        try {

            let notes
            if (req.body.notes) {
                notes = req.body.notes
            }
            const dataPayment = await payment.findOneAndUpdate({
                'user._id': new ObjectId(req.body.idUser),
            }, {
                $set: {
                    'status': "declined",
                    'notes': notes
                }
            }, {
                new: true
            })
            if (dataPayment) {
                return res.status(200).json({
                    "status": "success",
                    "message": "this payment has been declined, please upload again!",
                    "data": dataPayment
                })
            } else {
                return res.status(404).json({
                    "status": "failed",
                    "message": "Data Not Found",

                })
            }



        } catch (error) {
            console.log(error);
            return res.status(422).json({
                "status": "failed",
                "message": "Can't create a data",
                "data": error
            })
        }
    }

    //PAYMENT MIDTRANS
    async create_midtrans(req, res) {
        try {
            const dataUser = await user.findOne({
                '_id': req.user.id
            }, {
                '_id': 1,
                'fullName': 1,
                'email': 1,
                'image': 1
            })

            const dataPayment = await payment.create({
                'user': dataUser,
                'method': "midtrans",
                'status': "pending",
                'notes': "[payment by midtrans]"
            })


            const axios = require('axios');
            let data = JSON.stringify({
                "transaction_details": {
                    "order_id": `${dataPayment._id}`,
                    "gross_amount": 50000
                },
                "item_details": [{
                    "id": `UGD-${dataUser._id}`,
                    "name": "Upgrade Premium",
                    "price": 50000,
                    "quantity": 1
                }],
                "customer_details": {
                    "first_name": `${dataUser.fullName}`,
                    "email": `${dataUser.email}`
                },
                "expiry": {
                    "unit": "minutes",
                    "duration": 60
                }
            });

            var config = {
                method: 'post',
                url: 'https://app.sandbox.midtrans.com/snap/v1/transactions',
                headers: {
                    'Authorization': 'Basic U0ItTWlkLXNlcnZlci02QjMzbGxVblhmSmdmQ3I2NkM1aWM3QXg=',
                    'Content-Type': 'application/json'
                },
                data: data
            };

            const response = await axios(config)
            let newData = response.data;

            const getPayment = await payment.findOneAndUpdate({
                _id: dataPayment._id
            }, {
                $set: newData
            }, {
                new: true
            });

            return res.status(201).json({
                "status": "success",
                "message": "success for created payment by midtrans",
                "data": getPayment
            })

        } catch (e) {
            return res.status(422).json({
                "status": "failed",
                "message": "Can't create a data",
                "data": e
            })
        }
    }
   
    //UPDATE STATUS
    async update_status(req, res) {
        try {
            let data = req.query;
            let status;
            if (data.status_code == 200) {
                status = "successs";
            } else if (data.status_code == 201) {
                status = "pending";
            }

            let updatePayment = await payment.findOneAndUpdate({
                _id: data.order_id
            }, {
                $set: {
                    method: "midtrans",
                    status: status,
                    notes: "[already verified by midtrans]"
                }
            }, {
                new: true
            })
            const dataUser = await user.findOneAndUpdate({
                '_id': updatePayment.user._id
            }, {
                $set: {
                    'subscription': true
                }
            }, {
                new: true
            })
            return res.status(200).json({
                status: "success",
                message: "Success for update status",
                data: updatePayment,
                dataUser: dataUser
            });
        } catch (error) {
            return res.status(422).json({
                status: "failed",
                message: "can't update status"
            });
        }
    }

    //UPDATE STATUS POST
    async update_status_post(req, res) {
        try {
            let data = req.body;
            let status;

            if (data.status_code == 200) {
                status = "success";
            } else if (data.status_code == 201) {
                status = "pending";
            } else if (data.status_code == 407) {
                status = "expired"
            }

            let updatePayment = await payment.findOneAndUpdate({
                _id: data.order_id
            }, {
                $set: {
                    method: "midtrans",
                    status: status,
                    notes: "[already verified]"
                }
            }, {
                new: true
            })
            if (updatePayment.status == "success") {
                await user.findOneAndUpdate({
                    '_id': updatePayment.user._id
                }, {
                    $set: {
                        'subscription': true
                    }
                }, {
                    new: true
                })
            }

            return res.status(200).json({
                status: "success",
                message: "Success for update status payment",
                data: updatePayment
            });
        } catch (error) {
            return res.status(422).json({
                status: "failed",
                message: "can't update status"
            });
        }

    }
}
module.exports = new PaymentController;