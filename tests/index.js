let mongoose = require("mongoose"); // import mongoose
let {
    user,
    project,
    list,
    task
} = require('../models'); // import user models

//Require the dev-dependencies
let chai = require('chai'); // import chai for testing assert
let chaiHttp = require('chai-http'); // make virtual server to get/post/put/delete
let server = require('../index'); // import app from index
let should = chai.should(); // import assert should from chai
let token;
let tokenB;
let tokenC;
let idProject;
let idTask;
let destIdList;

chai.use(chaiHttp); // use chaiHttp

describe('User API', () => {
    before((done) => {
        user.deleteMany({

        }, (err) => {
            done();
        })
    })
    describe('/POST Sign Up', () => {
        it('It should make user and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/signup')
                .send({
                    email: 'teamgportra@gmail.com',
                    password: '987654321',
                    passwordConfirmation: '987654321',
                    fullName: "portra team g"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql('SignUp success! Please check your email');
                    res.body.should.have.property('token');
                    token = res.body.token
                    done()
                })
        })
        it('It should make second User and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/signup')
                .send({
                    email: 'sitish272@gmail.com',
                    password: '987654321',
                    passwordConfirmation: '987654321',
                    fullName: "siti"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql('SignUp success! Please check your email');
                    res.body.should.have.property('token');
                    tokenB = res.body.token
                    done()
                })
        })

        it('It should make third User and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/signup')
                .send({
                    email: 'zahlandnasution@gmail.com',
                    password: '987654321',
                    passwordConfirmation: '987654321',
                    fullName: "Sahlan"
                })
                .end((err, res) => {
                    res.should.have.status(200)
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql('SignUp success! Please check your email');
                    res.body.should.have.property('token');
                    tokenC = res.body.token
                    done()
                })
        })

    })

    describe('/VERIFY USER', () => {
        it('It should verify user', (done) => {
            chai.request(server)
                .get(`/user/verifyUser/${token}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status')
                    res.body.should.have.property('message').eql('Your email has been verified!');
                    done();
                })
        })
        it('It should verify second user', (done) => {
            chai.request(server)
                .get(`/user/verifyUser/${tokenB}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status')
                    res.body.should.have.property('message').eql('Your email has been verified!');
                    done();
                })
        })
        it('It should verify third user', (done) => {
            chai.request(server)
                .get(`/user/verifyUser/${tokenC}`)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status')
                    res.body.should.have.property('message').eql('Your email has been verified!');
                    done();
                })
        })
    })

    describe('/POST Login', () => {
        it('It should make user login and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/login')
                .send({
                    email: 'teamgportra@gmail.com',
                    password: '987654321'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql('Login Success!');
                    res.body.should.have.property('token');
                    token = res.body.token;
                    done()
                })
        })
        it('It should make user login and get authentication_key (jwt)', (done) => {
            chai.request(server)
                .post('/user/login')
                .send({
                    email: 'sitish272@gmail.com',
                    password: '987654321'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('message').eql('Login Success!');
                    res.body.should.have.property('token');
                    tokenB = res.body.token;
                    done()
                })
        })
    })

    describe('GET one', () => {
        it('It should get one user', (done) => {
            chai.request(server)
                .get('/user/get')
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success get the user data');
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    // token = res.body.token;
                    done();
                })
        })
    })

    describe('PUT UPDATE', () => {
        it('It should update the profile of the user', (done) => {
            chai.request(server)
                .put('/user/update')
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    fullName: 'sahlan n.',
                    bio: 'update profile',
                    image: 'image.jpg'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('message').eql('Success updating data!');
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                });
        })
    })

    describe('PUT change password', () => {
        it('It should change the password', (done) => {
            chai.request(server)
                .put('/user/change_password')
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    oldPassword: '987654321',
                    newPassword: '123456789',
                    reEnterPassword: '123456789'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('message').eql('Change password sucessfull');
                    done();
                });
        })
    })


})

describe('Project API', () => {
    before((done) => {
        project.deleteMany({}, (err) => {
            // done();
        })
        activity.deleteMany({}, (err) => {
            // done();
        })
        list.deleteMany({}, (err) => {
            done();
        })
    })

    /* POST CREATE PROJECT */
    describe('POST Create Project', () => {
        it('It should be return project that user created', (done) => {
            chai.request(server)
                .post('/project/create')
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    name: "Unit Testing",
                    description: "testing code"
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success for created project`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    idProject = res.body.data._id;
                    done();
                })

        })
    })

    /* GET USER'S PROJECT */
    describe('GET User Project', () => {
        it('It should get all project that user include', (done) => {
            chai.request(server)
                .get('/project/user')
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`succes get user's project`);
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('array')
                    done();
                })

        })
    })

    /* GET ONE PROJECT */
    describe("GET One Project", () => {
        it('It sould get one project by id', (done) => {
            chai.request(server)
                .get(`/project/getOne/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success for get one data`);
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done();
                })
        })
    })

    /* UPDATE PROJECT */
    describe("UPDATE One Project", () => {
        it('It sould update project name or desc', (done) => {
            chai.request(server)
                .put(`/project/update/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    name: "Unit Testing Update",
                    description: "update description Unit Testing"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`data has been updated`);
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done();
                })
        })
    })

    /*  INVITE MEMBER*/
    describe('INVITE Member', () => {
        it('Its should return the updated project with new member', (done) => {
            chai.request(server)
                .post(`/project/members/invite/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`

                })
                .send({
                    email: 'sitish272@gmail.com'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success for invite member`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                })
        })
    })

    /*  INVITE MEMBER ALREADY MEMBERS IN PROJECT*/
    describe('INVITE Member Already Members in Project', () => {
        it('Its should return Error Already Members in Project', (done) => {
            chai.request(server)
                .post(`/project/members/invite/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`

                })
                .send({
                    email: 'sitish272@gmail.com'
                })
                .end((err, res) => {
                    res.should.have.status(400);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('failed');
                    res.body.should.have.property('message').eql(`This email has join the project`);
                    done();
                })
        })
    })

    /*  KICK MEMBER*/
    describe('KICK Member', () => {
        it('Its should return the updated project without member', (done) => {
            chai.request(server)
                .delete(`/project/member/kick/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`

                })
                .send({
                    email: 'sitish272@gmail.com'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success to kick member`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                })
        })
    })

    describe('INVITE Member', () => {
        it('Its should return the updated project with new member', (done) => {
            chai.request(server)
                .post(`/project/members/invite/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`

                })
                .send({
                    email: 'sitish272@gmail.com'
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success for invite member`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                })
        })
    })



})
describe('List API', () => {
    let idList;
    describe("GET Project List", () => {
        it('It should return basic list', (done) => {
            chai.request(server)
                .get(`/list/getByProject/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success for get list by project`);
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('array')
                    done();
                })
        })
    })
    describe("POST Create List", () => {
        it('It should return basic new list', (done) => {
            chai.request(server)
                .post(`/list/create/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    name: "newList"
                })
                .end((err, res) => {
                    res.should.have.status(201);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success for created List`);
                    res.body.should.have.property('data')
                    idList = res.body.data._id
                    done();
                })
        })
    })
    describe("PUT Update List", () => {
        console.log(idList);
        it('It should return new list', (done) => {
            chai.request(server)
                .put(`/list/update/${idList}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    name: "List Update"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`success for update list`);
                    res.body.should.have.property('data')
                    res.body.data.should.be.an('object')
                    done();
                })
        })
    })
})

describe('Task API', () => {
    before((done) => {
        task.deleteMany({}, (err) => {
            done();
        })
    })
    /* POST CREATE TASK */
    describe('POST Create Task', () => {
        it('It should be return task that user created', (done) => {
            chai.request(server)
                .post(`/task/create/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    name: "Task Unit Testing",
                    description: "Task testing description"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`Task has been created!`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    idTask = res.body.data._id;
                    done();
                })

        })
    })

    /* GET ONE TASK */
    describe('GET One Task', () => {
        it('It should be Get One Task', (done) => {
            chai.request(server)
                .get(`/task/getTask/${idTask}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`Success to Get Task`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('array');
                    done();
                })

        })
    })

    /* GET ALL TASK */
    describe('GET All Task', () => {
        it('It should be Get All Task by idProject', (done) => {
            chai.request(server)
                .get(`/task/getAllTask/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`Success to Get All Task For This Project!`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    destIdList = Object.keys(res.body.data)[1]
                    done();
                })

        })
    })

    /* GET ALL TASK Error*/
    // describe('GET All Task', () => {
    //     it('It should be Get All Task by idProject', (done) => {
    //         chai.request(server)
    //             .get(`/task/getAllTask/${idProject}`)
    //             .set({
    //                 Authorization: `Bearer ${token}`
    //             })
    //             .end((err, res) => {
    //                 res.should.have.status(200);
    //                 res.body.should.be.an('object');
    //                 res.body.should.have.property('status').eql('success');
    //                 res.body.should.have.property('message').eql(`Success to Get Task`);
    //                 res.body.should.have.property('data');
    //                 res.body.data.should.be.an('object');
    //                 done();
    //             })

    //     })
    // })

    /* PUT UPDATE TASK */
    describe('PUT Update Task', () => {
        it('It should be update Task name and description', (done) => {
            chai.request(server)
                .put(`/task/update/${idTask}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    name: "This is update Task Name",
                    description: "This updated task description"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`Success Updating Task`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                })

        })
    })

    /* PUT ASSIGN TASK ERROR CANNOT ASSIGN */
    describe('PUT Assign Task Cannot Assign Task', () => {
        it('It should be return Error Cannot Assign Member to Task', (done) => {
            chai.request(server)
                .put(`/task/assignTask/${idTask}`)
                .set({
                    Authorization: `Bearer ${tokenB}`
                })
                .send({
                    email: "sitish272@gmail.com"
                })
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('failed');
                    res.body.should.have.property('message').eql(`You're not allowed to assign member for this project. Please contact you project owner!`);
                    done();
                })

        })
    })

    /* PUT ASSIGN TASK ERROR CANNOT ASSIGN */
    describe('PUT Assign Task Cannot Assign Task', () => {
        it('It should be return Error Cannot Assign Member to Task', (done) => {
            chai.request(server)
                .put(`/task/assignTask/${idTask}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    email: "sahlan.nasution07@gmail.com"
                })
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('failed');
                    res.body.should.have.property('message').eql(`User that you want to assign is not registered!`);
                    done();
                })

        })
    })

    /* PUT ASSIGN TASK ERROR NOT MEMBER PROJECT */
    describe('PUT Assign Task Not Member Project', () => {
        it('It should be return Error Not Member Project', (done) => {
            chai.request(server)
                .put(`/task/assignTask/${idTask}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    email: "zahlandnasution@gmail.com"
                })
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('failed');
                    res.body.should.have.property('message').eql(`This users is not a member of this project!`);
                    done();
                })

        })
    })

    /* PUT ASSIGN TASK */
    describe('PUT Assign Task', () => {
        it('It should be Assign Member to Task', (done) => {
            chai.request(server)
                .put(`/task/assignTask/${idTask}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    email: "sitish272@gmail.com"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`Success Assign Member to Task`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                })

        })
    })


    /* PUT ASSIGN TASK ERROR ALREADY ASSIGN */
    describe('PUT Assign Task Already Assign Task', () => {
        it('It should be return Error Already Assign Member to Task', (done) => {
            chai.request(server)
                .put(`/task/assignTask/${idTask}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    email: "sitish272@gmail.com"
                })
                .end((err, res) => {
                    res.should.have.status(422);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('failed');
                    res.body.should.have.property('message').eql(`Users has been assign in this task!`);
                    done();
                })

        })
    })



    /* PUT REVOKE TASK */
    describe('PUT Revoke Task', () => {
        it('It should be Revoke Member from Task', (done) => {
            chai.request(server)
                .put(`/task/revokeAssignTask/${idTask}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    email: "sitish272@gmail.com"
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`Success Revoke Member from Task`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                })

        })
    })

    /* MoVE TASK */
    describe('PUT Move Task', () => {
        it('It should be update Task position and list', (done) => {
            chai.request(server)
                .put(`/task/moveTask`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .send({
                    idTask: idTask,
                    destIdList: destIdList,
                    destIndex: 1
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`Success To Move Task`);
                    res.body.should.have.property('data');
                    res.body.data.should.be.an('object');
                    done();
                })

        })
    })

    /* DELETE PROJECT */
    describe("DELETE Project", () => {
        it('It sould deleted by id', (done) => {
            chai.request(server)
                .delete(`/project/delete/${idProject}`)
                .set({
                    Authorization: `Bearer ${token}`
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.an('object');
                    res.body.should.have.property('status').eql('success');
                    res.body.should.have.property('message').eql(`data has been deleted`);
                    res.body.should.have.property('data')
                    done();
                })
        })
    })
})