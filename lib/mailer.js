const nodemailer = require('nodemailer');
require('dotenv').config({
    path: `.env.${process.env.NODE_ENV}`
})
const transporter = nodemailer.createTransport({
    host: process.env.NODEMAILER_HOST,
    port: process.env.NODEMAILER_PORT,
    secure: true,
    auth: {
        user: process.env.NODEMAILER_EMAIL,
        pass: process.env.NODEMAILER_PASSWORD
    },
    tls: {
        rejectUnauthorized: false
    }
})

class Mailer {
    async verificationEmail(data, token) {
        const mailOptions = {
            from: process.env.NODEMAILER_EMAIL,
            to: data.email,
            subject: 'Account Verification Token',
            html: `
            <h3>Hi ${data.fullName}!</h3>
            <p>To start your amazing journey with us, please verify your account by click this link below</p>
            <a href='https://protra.netlify.app/user/verifyUser/${token}'>Verify Account</a>
            <p>Hope to see you soon!</p>
            <p>Thankyou!</p>`
        };

        const sendEmail = await transporter.sendMail(mailOptions);
        return sendEmail;
    }

    async inviteMember(invitee, invited, invitedfullName, project) {
        const mailOptions = {
            from: process.env.NODEMAILER_EMAIL,
            to: invited,
            subject: 'PROTRA : Invitation To The Project',
            html: `
                <h3>Hi ${invitedfullName}!</h3>
                <p>You've invited by ${invitee} in ${project}</p>
                <a href='https://protra.netlify.app'>Your Dashboard</a>
                <p>Hope to see you soon!</p>
                <p>Thankyou!</p>`
        };

        const sendEmail = await transporter.sendMail(mailOptions);
        return sendEmail;
    }

    async kickMember(invited, invitedfullName, project) {
        const mailOptions = {
            from: process.env.NODEMAILER_EMAIL,
            to: invited,
            subject: 'PROTRA Official : Kicked From The Project',
            html: `
                    <h3>Hi ${invitedfullName}!</h3>
                    <p>We apologize for this, You're kicked from ${project}</p>
                    <p>It's okay. Don't be sad, Let's Start Your Own Project by Click This Link Below</p>
                    <a href='https://protra.netlify.app'>Protra Website</a>
                    <p>Hope to see you soon!</p>
                    <p>Thankyou!</p>`
        };

        const sendEmail = await transporter.sendMail(mailOptions);
        return sendEmail;
    }
}

module.exports = new Mailer;