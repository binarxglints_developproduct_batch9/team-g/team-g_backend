module.exports = () => {
    let array = [
        'https://images.unsplash.com/photo-1591394262835-d04983289f2e?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1592363910020-d7c993729c5a?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1591251786031-33d3e775d1e9?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1591610265969-70b99bc29ade?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1592382419341-a1ef0ddd7c8b?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1593038065120-5f3bd95bbe0c?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1592234074116-11eeb7dab649?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1591889684709-b3c409221629?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1593349480374-01bb7d5782d5?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600',
        'https://images.unsplash.com/photo-1591806491151-73a404777045?crop=entropy&cs=tinysrgb&fit=crop&fm=jpg&h=900&ixlib=rb-1.2.1&q=80&w=1600'
    ]
    const random = Math.floor(Math.random() * Math.floor(10))
    return array[random]
}
