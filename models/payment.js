const mongoose = require('mongoose') // Impoer mongoose
const mongoose_delete = require('mongoose-delete') // Import mongoose-delete for enable softdelete
const bcrypt = require('bcrypt')

const PaymentSchema = new mongoose.Schema({
    // Define column
    user: {
        type: mongoose.Schema.Types.Mixed,
        required: [true, 'Payment must have an user'],
    },
    //bukti tranfer
    image: {
        type: String,
        required: false
    },
    //success, pending, failed
    status: {
        type: String,
        required: false,
        default: null
    },
    method: {
        type: String,
        required: false,
        default: null
    },
    notes: {
        type: String,
        required: false,
        default: null
    },
    // Additional for Midtrans (payment gateway)
    token: {
        type: String
    },
    redirect_url: {
        type: String
    }
    // end of additional for Midtrans
}, {
    // timestamps enable
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false // Disable versioning __v column
})

PaymentSchema.plugin(mongoose_delete, {
    overrideMethods: 'all'
}) // Enable softdelete

module.exports = payment = mongoose.model('payment', PaymentSchema, 'payment'); // Export Payment model