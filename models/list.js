const mongoose = require('mongoose') // Impoer mongoose
const mongoose_delete = require('mongoose-delete') // Import mongoose-delete for enable softdelete


const ListSchema = new mongoose.Schema({
    // Define column
    name: {
        type: String,
        required: [true, 'List must have an name']
    },
    idProject: {
        type: mongoose.Schema.Types.Mixed,
        required: [true, 'List must have idProject'],

    },
    position: {
        type: Number,
        required: [true, 'List must have position']
    }
}, {
    // timestamps enable
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false // Disable versioning __v column
})

ListSchema.plugin(mongoose_delete, {
    overrideMethods: 'all'
}) // Enable softdelete

module.exports = list = mongoose.model('list', ListSchema, 'list'); // Export List model