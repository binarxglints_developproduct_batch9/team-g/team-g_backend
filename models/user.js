const mongoose = require('mongoose') // Impoer mongoose
const mongoose_delete = require('mongoose-delete') // Import mongoose-delete for enable softdelete
const bcrypt = require('bcrypt')

const UserSchema = new mongoose.Schema({
    // Define column
    email: {
        type: String,
        required: [true, 'User must have an email'],
    },
    password: {
        type: String,
        required: [true, 'User must have password'],
        set: v => bcrypt.hashSync(v, 10)
    },
    fullName: {
        type: String,
        required: [true, 'User must have name'],
    },
    bio: {
        type: String,
        required: false,
        default: null
    },
    image: {
        type: String,
        required: false,
        default: "https://ik.imagekit.io/jx232h3mzoq/BlueHead_JkxqLVWc7_T2yS_W40n.jpg"
    },
    isVerified: {
        type: Boolean,
        required: true,
        default: false
    },
    subscription: {
        type: Boolean,
        required: true,
        default: false
    },
    // project: [{ type: mongoose.Schema.Types.ObjectId, ref: "Project"}]
}, {
    // timestamps enable
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false // Disable versioning __v column
})

UserSchema.plugin(mongoose_delete, {
    overrideMethods: 'all'
}) // Enable softdelete

module.exports = user = mongoose.model('user', UserSchema, 'user'); // Export User model