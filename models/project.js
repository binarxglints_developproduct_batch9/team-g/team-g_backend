const mongoose = require('mongoose') // Impoer mongoose
const mongoose_delete = require('mongoose-delete') // Import mongoose-delete for enable softdelete
const bcrypt = require('bcrypt')

const ProjectSchema = new mongoose.Schema({
    // Define column
    name: {
        type: String,
        required: [true, 'Project must have an name'],
    },
    description: {
        type: String,
        required: [true, 'Project must have description'],
    },
    owner:{
        type:mongoose.Schema.Types.Mixed,
        // ref: "user",
        required:[true,'Project must have owner']
    },
    bgImage: {
        type: String,
        required: false,
        default: null
    },
    members: {
        type: mongoose.Schema.Types.Mixed,
        // ref: "user",
        required: false,
    }
    // members: [{ type: mongoose.Schema.Types.ObjectId, ref: "User"}],
    // owner: [{ type: mongoose.Schema.Types.ObjectId, ref: "User"}]
}, {
    // timestamps enable
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false // Disable versioning __v column
})

ProjectSchema.plugin(mongoose_delete, {
    overrideMethods: 'all'
}) // Enable softdelete

module.exports = project = mongoose.model('project', ProjectSchema,'project'); // Export Project model
