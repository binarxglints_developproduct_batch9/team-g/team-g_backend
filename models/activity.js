const mongoose = require('mongoose') // Impoer mongoose
const mongoose_delete = require('mongoose-delete') // Import mongoose-delete for enable softdelete


const ActivitySchema = new mongoose.Schema({
    // Define column
    idUser:{
        type:mongoose.Schema.Types.Mixed,
        required:[true,'Activity must have idProject'],
    },
    idProject:{
        type:mongoose.Schema.Types.Mixed,
        required:[true,'Activity must have idProject'],

    },
    content:{
        type:String,
        required:[true,'Activity must have Content'],

    },
    isRead:{
        type:Boolean,
        required:[true,'Activity must have status read'],
        default:false
    }

}, {
    // timestamps enable
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false // Disable versioning __v column
})

ActivitySchema.plugin(mongoose_delete, {
    overrideMethods: 'all'
}) // Enable softdelete

module.exports = activity = mongoose.model('activity', ActivitySchema, 'activity'); // Export Activity model
