const mongoose = require('mongoose') // Impoer mongoose
const mongoose_delete = require('mongoose-delete') // Import mongoose-delete for enable softdelete
const bcrypt = require('bcrypt')

const TaskSchema = new mongoose.Schema({
    // Define column
    name: {
        type: String,
        required: [true, 'Task must have an name'],
    },
    description: {
        type: String,
        required: [true, 'Task must have description'],
    },
    attachment: {
        type: mongoose.Schema.Types.Mixed,
        required: false,
        default: []
    },
    members: {
        type: mongoose.Schema.Types.Mixed,
        required: false,
        default: []
    },
    idProject: {
        type: mongoose.Schema.Types.Mixed,
        required: [true, 'Task must have idProject']
    },
    idList: {
        type: mongoose.Schema.Types.Mixed,
        required: [true, 'Task must have idList']
    },
    position:{
        type: Number,
        required: [true, 'Task must have position']
    }
}, {
    // timestamps enable
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    versionKey: false // Disable versioning __v column
})

TaskSchema.plugin(mongoose_delete, {
    overrideMethods: 'all'
}) // Enable softdelete

module.exports = task = mongoose.model('task', TaskSchema, 'task'); // Export Task model