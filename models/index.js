const path = require('path');
require('dotenv').config({
    path: `.env.${process.env.NODE_ENV}`
})

const mongoose = require('mongoose') // Import mongoose

// const uri = "mongodb+srv://teamgportra:AGn7uW1xFH71fTgF@finalprojectcluster.bwl3k.mongodb.net/protra_development?retryWrites=true&w=majority" // Database url in mongodb
// const uri = "mongodb://localhost:27017/protra_development"
// const uri = "mongodb://localhost:27017/protra" // Database url in mongodb
const uri = process.env.MONGO_URI // Database url in mongodb

mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false
}) // Make connection to mongodb protra_development database


const user = require('./user') // import user model
const project = require('./project') // import project model
const list = require('./list') // import list model
const task = require('./task') // import task model
const activity = require('./activity') // import activity model
const payment = require('./payment') // import payment model
// const admin = require('./admin') // import admin model

//export all models collection
module.exports = {
    user,
    project,
    list,
    task,
    activity,
    payment
}