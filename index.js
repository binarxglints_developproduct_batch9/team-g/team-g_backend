const express = require('express'); // import express
const app = express(); // activate app
const bodyParser = require('body-parser'); // import body-parser
const cors = require('cors') // import cors
const passport = require('passport')
require('./services/googlePassport')
require('dotenv').config({
  path: `.env.${process.env.NODE_ENV}`
})
const server = require('http').Server(app);

// const server = app.listen(3000);
// const io = require('socket.io').listen(server);

//cors=>no need, already installed
// app.use(function(req, res, next) {
//   let allowedOrigins = ['*']; // list of url-s
//   let origin = req.headers.origin;
//   if (allowedOrigins.indexOf(origin) > -1) {
//     res.setHeader('Access-Control-Allow-Origin', origin);
//   }
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   res.header('Access-Control-Expose-Headers', 'Content-Disposition');
//   next();
// });

app.use(passport.initialize());
app.use(passport.session())

/* Put SSL */
// const fs = require("fs");
// const https = require("https");
// const key = fs.readFileSync("./ssl/key.pem", "utf-8");
// const cert = fs.readFileSync("./ssl/cert.pem", "utf-8");

/* End of routes */

/* Put all routes below here */
const userRoutes = require('./routers/userRoutes')
const projectRoutes = require('./routers/projectRoutes')
const listRoutes = require('./routers/listRoutes')
const taskRoutes = require('./routers/taskRoutes')
const paymentRoutes = require('./routers/paymentRoutes')
// const adminRoutes = require('./routers/adminRoutes')
// const voteRoutes = require('./routers/voteRoutes')
const activityRoutes = require('./routers/activityRoutes')
/* End of routes */


app.use(cors()) // activate cors
const io = require('socket.io')(server, {
  cors: {
    origin: '*',
  }
});
app.use(async function (req, res, next) {
  req.io = io;
  next();
});

//Set body parser for HTTP post operation
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies

//directory public
app.use(express.static('public'));
// app.use('/', voteRoutes);


/* Using all routes below here */
app.use('/user', userRoutes)
app.use('/project', projectRoutes)
app.use('/list', listRoutes)
app.use('/task', taskRoutes)
app.use('/payment', paymentRoutes)
// app.use('/admin', adminRoutes)
app.use('/activity', activityRoutes)
/* End of use routes */


/*
Socket.io Setting
*/
io.on('connection', async function (socket) {

  console.log(socket.id + " connected!");
  socket.on('join', (msg) => {
    console.log(msg);

    socket.join(msg.room);
  });


  socket.on("disconnect", (reason) => {
    console.log(socket.id + " disconnected because " + reason);
  });
});

server.listen(3000, () => console.log("server running on http://localhost:3000"))


module.exports = server