require('dotenv').config({
    path: `.env.${process.env.NODE_ENV}`
})
const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const mongoose = require('mongoose')
const User = require('../models/user')


passport.serializeUser((user, done) => {
    done(null, user.id)
})

passport.deserializeUser((id, done) => {
    User.findById(id).then((user) => {
        done(null, user)
    })
})

passport.use(
    new GoogleStrategy({
            clientID:process.env.GOOGLE_CLIENT_ID,
            clientSecret:process.env.GOOGLE_CLIENT_SECRET,
            callbackURL: "/user/auth/google/callback"
        },
        (accessToken, refreshToken, profile, done) => {
        //     console.log(profile);
        //    console.log('token:'+accessToken);
            User.findOne({
                    email: profile._json.email
                })
                .then((existingUser) => {
                    if (existingUser) {
                        done(null, existingUser)
                    } else {
                        new User({
                                fullName: profile.displayName,
                                email: profile._json.email,
                                image: profile._json.picture,
                                password:'defaultPassword',
                                isVerified:true

                            }).save()
                            .then((user) => {
                                done(null, user)
                            })
                    }
                })


        }
    )

)
