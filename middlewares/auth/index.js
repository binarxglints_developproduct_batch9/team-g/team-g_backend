const passport = require('passport'); // Import passport
const localStrategy = require('passport-local').Strategy; // Import Strategy
const nodemailer = require('nodemailer')

const {
    user
} = require('../../models') // Import user model
const bcrypt = require('bcrypt'); // Import bcrypt
const JWTstrategy = require('passport-jwt').Strategy; // Import JWT Strategy
const ExtractJWT = require('passport-jwt').ExtractJwt; // Import ExtractJWT


/* user signup */
passport.use(
    'signup',
    new localStrategy({
            'usernameField': 'email', // field for username from req.body.email
            'passwordField': 'password', // field for password from req.body.password
            passReqToCallback: true // read other requests
        },
        async (req, email, password, done) => {
            // Create user data
            user.create({
                fullName: req.body.fullName,
                email: email, // email get from usernameField (req.body.email)
                password: password, // password get from passwordField (req.body.passport)
            }).then(result => {
                // If success, it will return authorization with req.user
                const data = {
                    id: result._id,
                    email: result.email,
                    fullName: result.fullName
                }
                return done(null, data, {
                    message: 'User created!'
                })
            }).catch(err => {
                // If error, it will return not authorization
                return done(null, false, {
                    message: "User can't be created"
                })
            })
        },
    )
);

/* Email varification */
passport.use(
    'verifyUser',
    new JWTstrategy({
            secretOrKey: 'secret_password', // key for jwt
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
        },
        async (token, done) => {
            // find user depend on token.user.email

            const checkUser = await user.findOne({
                email: token.user.email
            })

            const data = token.user
            // if user.role includes user it will next
            if (checkUser) {
                await user.updateOne({
                    email: token.user.email
                }, {
                    $set: {
                        isVerified: true
                    }
                })
                return done(null, data)
            }

            // if user.role not includes user it will not authorization
            return done(null, false)
        }
    )
);

/* user login */
passport.use(
    'login',
    new localStrategy({
            'usernameField': 'email', // username from req.body.email
            'passwordField': 'password', // password from req.body.password
        },
        async (email, password, done) => {
            // find user depends on email
            const userLogin = await user.findOne({
                email: email

            })

            // if user not found
            if (!userLogin) {
                return done(null, false, {
                    message: 'User is not found!'
                })
            }

            // if user found and validate the password of user
            const validate = await bcrypt.compare(password, userLogin.password);

            // if wrong password
            if (!validate) {
                return done(null, false, {
                    message: 'Wrong password!'
                })
            }

            // login success
            return done(null, userLogin, {
                message: 'Login success!'
            })
        }
    )
);

// USER AUTH
passport.use(
    'user',
    new JWTstrategy({
            secretOrKey: 'secret_password', // key for jwt
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
        },
        async (token, done) => {
            // find user depend on token.user.email
            const userLogin = await user.findOne({
                email: token.user.email
            })
            // console.log(token.user.email);
            // if user.role includes user it will next
            if (userLogin) {
                return done(null, token.user)
            }

            // if user.role not includes user it will not authorization
            return done(null, false)

        }
    )
)

// ADMIN AUTH
passport.use(
    'admin',
    new JWTstrategy({
            secretOrKey: 'secret_password', // key for jwt
            jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken() // extract token from authorization
        },
        async (token, done) => {
            // find admin depend on token.admin.email
            const adminLogin = await admin.findOne({
                email: token.admin.email
            })
            // console.log(token.admin.email);
            // if admin.role includes admin it will next
            if (adminLogin) {
                return done(null, token.admin)
            }

            // if admin.role not includes admin it will not authorization
            return done(null, false)

        }
    )
)
/* admin login */
passport.use(
    'login_admin',
    new localStrategy({
            'usernameField': 'email', // username from req.body.email
            'passwordField': 'password', // password from req.body.password
        },
        async (email, password, done) => {
            // find user depends on email
            const adminLogin = await admin.findOne({
                email: email

            })

            // if user not found
            if (!adminLogin) {
                return done(null, false, {
                    message: 'User is not found!'
                })
            }

            // if user found and validate the password of user
            const validate = await bcrypt.compare(password, adminLogin.password);

            // if wrong password
            if (!validate) {
                return done(null, false, {
                    message: 'Wrong password!'
                })
            }

            // login success
            return done(null, adminLogin, {
                message: 'Login success!'
            })
        }
    )
);