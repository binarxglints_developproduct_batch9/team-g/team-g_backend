const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator'); //form validation & sanitize form params
const {
    user,
    task,
    project,
    list
} = require('../../models');
const {
    ObjectId
} = require('mongodb');


module.exports = {
    create: [
        check('idProject').custom(value => {
            // console.log(value);
            return project.findOne({
                '_id': value
            }).then(result => {
                if (!result) {
                    throw new Error("ID project not found")
                }
            })
        }),
        check('name', "List must have name").notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ],
    update: [
        check('idList').custom(value => {
            // console.log(value);
            return list.findOne({
                '_id': value
            }).then(result => {
                if (!result) {
                    throw new Error("ID List not found")
                }
            })
        }),
        check('name', "List must have name").notEmpty(),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                })
            }
            next();
        }
    ],

};