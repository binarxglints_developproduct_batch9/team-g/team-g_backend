const {
  user,
  project
} = require('../../models');
const {
  check,
  matchedData,
  validationResult,
  sanitize
} = require('express-validator');
const {
  ObjectId
} = require('mongodb')


const multer = require('multer'); //multipar form-data


const upload = multer({
  fileFilter: (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/jpg' || file.mimetype == 'image/png')
      cb(null, true)
    else {
      cb(null, false)
      return cb(new Error('Only .jpeg or .jpg or .png are allowed to upload'))
    }
  }
});

module.exports = {
  getOne: [
    check('idProject').custom(value => {
      return project.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error("ID project doesn't exist")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  userProject: [
    check('id').custom((value, {
      req
    }) => {
      return user.findOne({
        '_id': req.user.id
      }).then(b => {
        if (!b) {
          throw new Error('No data user !');
        }

      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        console.log("cek validator");
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],
  create: [
    // check('name', 'name must be a string').isString(),
    // check('description', 'description must be a string').isString(),
    check('id').custom((value, {
      req
    }) => {

      return user.findOne({
        '_id': req.user.id
      }).then(b => {
        if (!b) {
          throw new Error('No data user !');
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  update: [
    check('idProject').custom(value => {
      return project.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error("ID project doesn't exist")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  delete: [
    check('idProject').custom(value => {
      return project.findOne({
        _id: value
      }).then(result => {
        if (!result) {
          throw new Error("ID project doesn't exist")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  inviteMember: [
    check('idProject').custom(value => {
      //console.log(value);
      return project.findOne({
        "_id": value
      }).then(result => {
        if (!result) {
          throw new Error("invalid ID Project")
        }
      })
    }),
    check('email').custom(value => {
      return user.findOne({
        'email': value
      }).then(result => {
        if (!result) {
          throw new Error("this email migt be not registered")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  kickMember: [
    check('idProject').custom(value => {
      //console.log(value);
      return project.findOne({
        '_id': value
      }).then(result => {
        if (!result) {
          throw new Error("ID project doesn't exist")
        }
      })
    }),
    check('email').custom(value => {
      return user.find({
        'email': value
      }).then(result => {
        if (!result) {
          throw new Error("this email might be not registered")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  inviteMembers: [
    check('idProject').custom(value => {
      //console.log(value);
      return project.findOne({
        "_id": value
      }).then(result => {
        if (!result) {
          throw new Error("invalid ID Project")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  leave: [
    check('idProject').custom(value => {
      //console.log(value);
      return project.findOne({
        "_id": value
      }).then(result => {
        if (!result) {
          throw new Error("invalid ID Project")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  upload: upload


}