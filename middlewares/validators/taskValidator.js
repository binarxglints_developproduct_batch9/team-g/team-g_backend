const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params
const {
  user,
  task,
  project,
  list
} = require('../../models');
const {
  ObjectId
} = require('mongodb');

const multer = require('multer'); //multipar form-data
const upload = multer({
  fileFilter: (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/jpg' || file.mimetype == 'image/png')
      cb(null, true)
    else {
      cb(null, false)
      return cb(new Error('Only .jpeg or .jpg or .png are allowed to upload'))
    }
  }
});

module.exports = {
  create: [
    check('idProject').custom(value => {
      if (value) {
        return project.findOne({
          _id: value
        }).then(result => {
          if (!result) {
            throw new Error("ID project doesn't exist")
          }
        })
      }
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        })
      }
      next();
    }
  ],
  update: [
    //Set form validation rule
    check('id').custom((value, {
      req
    }) => {
      return task.findOne({
        _id: req.params.idTask
      }).then(result => {
        if (!result) {
          throw new Error("ID task does not exist")
        }
      })
    }),
    // check('title').isString(),
    // check('description').isString(),
    // check('attachment').isArray(),
    // check('members').isArray().custom(value => {
    //   return user.findOne({
    //     email: req.user.email
    //   }).then(result => {
    //     if (!result) {
    //       throw new Error("Email's member has not been registered")
    //     }
    //   })
    // }),

    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          errors: errors.mapped()
        });
      }
      next();
    },
  ],

  upload: upload,

  assignTask: [
    check('idTask').custom(value => {
      // console.log(value);
      return task.findOne({
        '_id': value
      }).then(result => {
        if (!result) {
          throw new Error("ID task does not exist")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          status: "failed",
          message: errors.mapped()
        });
      }
      next();
    },
  ],

  revokeAssignTask: [
    check('idTask').custom(value => {
      // console.log(value);
      return task.findOne({
        '_id': value
      }).then(result => {
        if (!result) {
          throw new Error("ID task does not exist")
        }
      })
    }),
    (req, res, next) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({
          status: "failed",
          message: errors.mapped()
        });
      }
      next();
    },
  ]
};