const bcrypt = require("bcrypt");
const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator'); //form validation & sanitize form params
const {
    admin
} = require('../../models');

module.exports = {
    signup: [
        check('username', 'full name must be filled with characters').isString().notEmpty(),
        check('email', 'email field must be email address ').isString().custom(value => {
            return admin.findOne({
                email: value
            }).then(b => {
                if (b) {
                    throw new Error('this email has been used, please use another email !');
                }
            })
        }), // email must be email
        check('password', 'password field must have 8 to 32 characters').isString().isLength({
            min: 8,
            max: 32
        }), // password must be 8 to 32 chars
        check('passwordConfirmation', 'passwordConfirmation field must have the same value as the password field').exists()
        .custom((value, {
            req
        }) => value === req.body.password),
        (req, res, next) => {
            console.log('check')
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    login: [
        check('email', 'email field must be email address').normalizeEmail().isEmail()
        .custom(value => {
            return admin.findOne({
                email: value
            }).then(mail => {
                if (!mail) {
                    throw new Error('this email has not been registered')
                }
                // if (mail.isVerified !== true) {
                //     throw new Error('this email has not yet been verified')
                // }
            })
        }),
        check('password', 'password field must have 8 to 32 characters').isString().isLength({
            min: 8,
            max: 32
        }), // password must be 8 to 32 chars
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    //GET ONE ADMIN
    getOne: [
        check('id').custom((value, {
            req
        }) => {
            // console.log(value)
            return admin.findOne({
                _id: value
            }).then(b => {
                if (!b) {
                    throw new Error('No data admin !');
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        },
    ],

    //DELETE ADMIN
    delete: [
        check('id').custom(value => {
          return admin.findOne({
            _id: value
          }).then(result => {
            if (!result) {
              throw new Error('No data admin !')
            }
          })
        }),
        (req, res, next) => {
          const errors = validationResult(req);
          if (!errors.isEmpty()) {
            return res.status(422).json({
              errors: errors.mapped()
            });
          }
          next();
        }
      ]
}