const bcrypt = require("bcrypt");
const {
    check,
    validationResult,
    matchedData,
    sanitize
} = require('express-validator'); //form validation & sanitize form params
const {
    user
} = require('../../models');

const multer = require('multer'); //multipar form-data
const path = require('path'); // to detect path of directory
const crypto = require('crypto'); // to encrypt something

const uploadDir = '/img/'; // make images upload to /img/
const storage = multer.diskStorage({
    destination: "./public" + uploadDir, // make images upload to /public/img/
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(16, function (err, raw) {
            if (err) return cb(err)

            cb(null, raw.toString('hex') + path.extname(file.originalname)) // encrypt filename and save it into the /public/img/ directory
        })
    }
})

const upload = multer({
    fileFilter: (req, file, cb) => {
        if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/jpg' || file.mimetype == 'image/png')
            cb(null, true)
        else {
            cb(null, false)
            return cb(new Error('Only .jpeg or .jpg or .png are allowed to upload'))
        }
    }
});

module.exports = {
    signup: [
        check('fullName', 'full name must be filled with characters').isString().notEmpty(),
        check('email', 'email field must be email address ').normalizeEmail().isEmail().custom(value => {
            return user.findOne({
                email: value
            }).then(b => {
                if (b) {
                    throw new Error('this email has been used, please use another email !');
                }
            })
        }), // email must be email
        check('password', 'password field must have 8 to 32 characters').isString().isLength({
            min: 8,
            max: 32
        }), // password must be 8 to 32 chars
        check('passwordConfirmation', 'passwordConfirmation field must have the same value as the password field').exists()
        .custom((value, {
            req
        }) => value === req.body.password),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],
    login: [
        check('email', 'email field must be email address').normalizeEmail().isEmail()
        .custom(value => {
            return user.findOne({
                email: value
            }).then(mail => {
                if (!mail) {
                    throw new Error('this email has not been registered')
                }
                if (mail.isVerified !== true) {
                    throw new Error('this email has not yet been verified')
                }
                // if(mail.subscription !== true) {
                //     throw new Error('you are not subscribed yet! Please pay the subscription fee')
                // }
            })
        }),
        check('password', 'password field must have 8 to 32 characters').isString().isLength({
            min: 8,
            max: 32
        }), // password must be 8 to 32 chars
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],

    getOne: [
        check('id').custom((value, {
            req
        }) => {
            return user.findOne({
                _id: req.user.id
            }).then(b => {
                if (!b) {
                    throw new Error('No data user !');
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        },
    ],

    update: [
        //Set form validation rule

        check('id').custom((value, {
            req
        }) => {
            return user.findOne({
                _id: req.user.id
            }).then(b => {
                if (!b) {
                    throw new Error('No data user !');
                }
            })
        }),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        },
    ],
    changePassword: [
        check('oldPassword', 'please insert your current password').isString().custom((value, {
            req,
            res
        }) => {
            return user.findOne({
                _id: req.user.id
            }).then(userData => {
                return bcrypt.compare(req.body.oldPassword, userData.password)
            }).then(checkUserPassword => {
                if (!checkUserPassword) {
                    throw new Error("your current password is wrong. Please enter the correct password!")
                }
            })
        }),
        check('newPassword', 'password field must have 8 to 32 characters').isString().isLength({
            min: 8,
            max: 32
        }), // password must be 8 to 32 characters
        check('reEnterPassword', 're-enter password field must have the same value as the new password field').exists()
        .custom((value, {
            req
        }) => value === req.body.newPassword),
        (req, res, next) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({
                    errors: errors.mapped()
                });
            }
            next();
        }
    ],
    setPassword:[
    check('password', 'password field must have 8 to 32 characters').isString().isLength({
        min: 8,
        max: 32
    }), // password must be 8 to 32 chars
    check('passwordConfirmation', 'passwordConfirmation field must have the same value as the password field').exists()
    .custom((value, {
        req
    }) => value === req.body.password),
    (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({
                errors: errors.mapped()
            });
        }
        next();
    },

  ],

    upload: upload,
};
