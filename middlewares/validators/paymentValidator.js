const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params
const {
  user,
  payment
} = require('../../models');
const {
  ObjectId
} = require('mongodb');

const multer = require('multer'); //multipar form-data


const upload = multer({
  fileFilter: (req, file, cb) => {
    if (file.mimetype == 'image/jpeg' || file.mimetype == 'image/jpg' || file.mimetype == 'image/png')
      cb(null, true)
    else {
      cb(null, false)
      return cb(new Error('Only .jpeg or .jpg or .png are allowed to upload'))
    }
  }
});

module.exports = {
  upload: upload
};