const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport auth strategy
const ProjectController = require('../controllers/projectController'); // import userController
const projectValidator = require('../middlewares/validators/projectValidator'); // import userValidator

//if you want to acces without auth
router.get('/kerasakti/getAll', ProjectController.getAll)


/**USER SIDE, need authenticate user*/

//get user's project
router.get('/user', passport.authenticate('user', {
    session: false
}), projectValidator.userProject, ProjectController.userProject)

//user get one of their project
router.get('/getOne/:idProject', passport.authenticate('user', {
    session: false
}), projectValidator.getOne, ProjectController.getOne)

//user create project
router.post('/create', [passport.authenticate('user', {
    session: false
}), projectValidator.create, projectValidator.upload.single("bgImage")], ProjectController.create)

//user update their project
router.put('/update/:idProject', passport.authenticate('user', {
    session: false
}), projectValidator.update, projectValidator.upload.single("bgImage"), ProjectController.update)

//only owner can delete their project
router.delete('/delete/:idProject', passport.authenticate('user', {
    session: false
}), projectValidator.delete, ProjectController.delete)


//owner and member can add members
router.get('/member/:idProject', passport.authenticate('user', {
    session: false
}), ProjectController.showMembersAndOwner)
router.post('/member/invite/:idProject', passport.authenticate('user', {
    session: false
}), projectValidator.inviteMember, ProjectController.inviteMember)
router.delete('/member/kick/:idProject', passport.authenticate('user', {
    session: false
}), projectValidator.kickMember, ProjectController.kickMember)
router.delete('/member/leave/:idProject', passport.authenticate('user', {
    session: false
}), projectValidator.leave, ProjectController.leave)
router.post('/members/invite/:idProject', [passport.authenticate('user', {
    session: false
}), projectValidator.inviteMembers], ProjectController.inviteMembers)

module.exports = router; // export router