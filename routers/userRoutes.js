const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport auth strategy
const UserController = require('../controllers/userController'); // import userController
const userValidator = require('../middlewares/validators/userValidator'); // import userValidator


// if user go to localhost:3000/signup
router.post('/signup', [userValidator.signup, function (req, res, next) {
    passport.authenticate('signup', {
        session: false
    }, function (err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            res.status(401).json({
                status: 'Error',
                message: info.message
            });
            return;
        }

        UserController.signup(user, req, res, next);
    })(req, res, next);
}]);

router.get('/verifyUser/:token', UserController.verifyUser)

// if user go to localhost:3000/login (signin)
router.post('/login', [userValidator.login, function (req, res, next) {
    passport.authenticate('login', {
        session: false
    }, async function (err, user, info) {
        if (err) {
            return next(err);

        }
        if (!user) {
            res.status(401).json({
                status: 'Error',
                message: info.message
            });
            return;
        }

        UserController.login(user, req, res);
    })(req, res, next);
}]);

router.get('/get', [passport.authenticate('user', {
    session: false
}), userValidator.getOne], UserController.getOne)

router.put('/update', [passport.authenticate('user', {
    session: false
}), userValidator.update, userValidator.upload.single("image")], UserController.update)

router.put('/change_password', [passport.authenticate('user', {
    session: false
})], userValidator.changePassword, UserController.changePassword)

/*CONTINUE WITH google*/

//user click signInGoogle req api
router.get('/auth/google', passport.authenticate('google', {
    session: false,
    scope: ["profile", "email"],
    // accessType: "offline",
    // approvalPrompt: "force"
}))

//redirect:get token
router.get("/auth/google/callback", passport.authenticate('google', {
    session: false
}), UserController.signInGoogle)

//set password if passwordnya still default
router.put('/setPassword/:token', userValidator.setPassword, UserController.setPassword);

//VERIFY GOOGLE LOGIN AFTER GET TOKEN FROM FE
router.post('/verify', UserController.readToken);

//if you want to acces without auth
router.get('/kerasakti/getAll', UserController.getAll)


module.exports = router; // export router