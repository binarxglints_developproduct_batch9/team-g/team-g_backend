const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport auth strategy
const TaskController = require('../controllers/taskController'); // import Controller
const taskValidator = require('../middlewares/validators/taskValidator'); // import userValidator



// GET ALL TASK IN PROJECT DETAIL
router.get('/getAllTask/:idProject', TaskController.getAllTask)

//GET TASK WHEN USER WANT TO EDIT
router.get('/getTask/:idTask', passport.authenticate('user', {
    session: false
}), TaskController.getTask)

//DELETE TASK
router.delete('/deleteTask/:idTask', passport.authenticate('user', {
    session: false
}), TaskController.deleteTask)

// CREATE TASK
router.post('/create/:idProject', [passport.authenticate('user', {
    session: false
}), taskValidator.create, taskValidator.upload.array('image')], TaskController.create)


//MOVE TASK LIST
router.put('/moveTaskList/:idTask', [passport.authenticate('user', {
    session: false
})], TaskController.moveTaskList)

//UPDATE TASK DETAIL
router.put('/update/:idTask', passport.authenticate('user', {
    session: false
}), taskValidator.update, TaskController.update)

//ATTACH FILE
router.put('/attachTask/:idTask', passport.authenticate('user', {
    session: false
}), taskValidator.upload.array('attachment'), TaskController.attachTask)

//ATTACH FILE
router.put('/deleteAttachment/:idTask', passport.authenticate('user', {
    session: false
}), TaskController.deleteAttachment)

//ASSIGN TASK 
router.put('/assignTask/:idTask', passport.authenticate('user', {
    session: false
}), taskValidator.assignTask, TaskController.assignTask)

// REVOKE ASSIGN TASK 
router.put('/revokeAssignTask/:idTask', passport.authenticate('user', {
    session: false
}), taskValidator.revokeAssignTask, TaskController.revokeAssignTask)


//COBA-COBA MOVE TASK
router.put('/moveTask', TaskController.moveTask)

router.get('/kerasakti/getAll', TaskController.getAll)

module.exports = router; // export router