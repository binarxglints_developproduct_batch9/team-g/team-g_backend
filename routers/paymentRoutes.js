const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport auth strategy
const PaymentController = require('../controllers/paymentController'); // import userController
const paymentValidator = require('../middlewares/validators/paymentValidator'); // import userValidator


//get all payment
router.get('/getAll', [passport.authenticate('admin', {
    session: false
}), paymentValidator.upload.single("image")], PaymentController.getAll)


//user want to send payment
// router.post('/upload', [passport.authenticate('user', {
//     session: false
// }), paymentValidator.upload.single("image")], PaymentController.create)

//admin verify 
// router.put('/verify', PaymentController.verify)
// router.put('/decline', PaymentController.decline)

//payment gateway (Midtrans)
router.post('/create_midtrans', [passport.authenticate('user', {
    session: false
})], PaymentController.create_midtrans)
router.get('/update_status', PaymentController.update_status);
router.post('/update_status_post', PaymentController.update_status_post);


//if you want to pass auth
router.get('/kerasakti/getAll', PaymentController.getAll)

module.exports = router; // export router