const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport auth strategy
const ListController = require('../controllers/listController'); // import userController
const listValidator = require('../middlewares/validators/listValidator'); // import userValidator

//if you want to acces without auth
router.get('/kerasakti/getAll', ListController.getAll)


//get all list of the project
router.get('/getByProject/:idProject', passport.authenticate('user', {
    session: false
}), ListController.getByProject)

//user create list
router.post('/create/:idProject', [passport.authenticate('user', {
    session: false
}), listValidator.create], ListController.create)

//user move all card in list
router.put('/moveList', passport.authenticate('user', {
    session: false
}), ListController.moveList)

//user update name of list
router.put('/update/:idList', [passport.authenticate('user', {
    session: false
}),listValidator.update], ListController.update)

//user delete list
router.delete('/delete/:idList', passport.authenticate('user', {
    session: false
}), ListController.delete)


module.exports = router; // export router