const express = require('express'); // import express
const router = express.Router(); // import router
const passport = require('passport'); // import passport
const auth = require('../middlewares/auth'); // import passport auth strategy
const ActivityController = require('../controllers/activityController'); // import adminController

//ADMIN GET ALL
router.get('/getAll', [passport.authenticate('admin', {
    session: false
})], ActivityController.getAll)


//ADMIN GET ONE
router.get('/userActivity', [passport.authenticate('user', {
    session: false
})], ActivityController.userActivity)



//if you want to acces without auth
router.get('/kerasakti/getAll', ActivityController.getAll)

module.exports = router; // export router